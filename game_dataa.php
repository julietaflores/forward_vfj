<?php
if( !isset( $_GET['id'] ) ){
    header('index.php');
}


include_once 'admin/connection.php';
include_once 'admin/models/model_fixture.php';
include_once 'admin/models/model_goal_scorer.php';
include_once 'admin/models/model_cautioned_player.php';
include_once ('admin/models/model_tournament.php');

$fixtureModel = new Fixture_Model();
$goalScorerModel = new Goal_Scorer_Model();

$tournamentModel = new Tournament_Model();

$match = $fixtureModel->getAll11f($_GET['id']);



foreach($match as $posicion=>$info)
{  $match['id_tournament']=$info['id_tournament'];
    $match['date_game_fixture']=$info['date_game_fixture'];
    $match['id_fixture']=$info['id_fixture'];
    $match['hour_game_fixture']=$info['hour_game_fixture'];
   $tipot = $info['id_tournament'];

   if(!empty($info['id_team_T1'])){
    $bb=$info['id_team_T1'];
    $bbn=$info['name_team_T1'];

}else{
    $bb=$info['id_team_1'];
    $bbn=$info['t1Name'];
}
if(!empty($info['id_team_T2'])){
    $bb1=$info['id_team_T2'];
    $bb1n=$info['name_team_T2'];
}else{
    $bb1=$info['id_team_2'];
    $bb1n=$info['t2Name'];
}


if(empty($bb) || empty($bb1)){
    header('Location: game_d.php?id='.$match['id_fixture']);
}




$List1 = $goalScorerModel->getAllai($match['id_tournament'], $info['id_fixture'],$bb1);
$List2 = $goalScorerModel->getAllai($match['id_tournament'], $info['id_fixture'],$bb);



}





$tournament = $tournamentModel->getById( $match['id_tournament']);

foreach($tournament as $posicion=>$tont)
{
   $name = $tont['name_tournament'];
}



 if(empty($List1)){
    $List1=0;

    if(empty($List2)){
        $List2=0;
    }else{
        $List2=count($List2);    
    }
 }else{
     $List1=count($List1);
     if(empty($List2)){
         $List2=0;
     }else{
         $List2=count($List2);    
     }
 }



 if($List1<$List2){
    $rt1=$bb1;
    $rt2=$bb1n;
    $rt3=$List1;
    $bb1=$bb;
    $bb1n=$bbn;
    $List1=$List2;
    $bb=$rt1;
    $bbn=$rt2;
    $List2=$rt3;

 }


 $goals1 = $goalScorerModel->getAllByFixtureByTeam1($match['id_fixture'], $bb1);
 $goals2 = $goalScorerModel->getAllByFixtureByTeam1($match['id_fixture'], $bb);


 $goalScorerList = $goalScorerModel->getAlla($match['id_tournament'], $_GET['id']);



$cautionedPlayerModel = new Cautioned_Player_Model();
$cautions1 = $cautionedPlayerModel->getAllByFixtureByTeam($_GET['id'], $bb1);
$cautions2 = $cautionedPlayerModel->getAllByFixtureByTeam($_GET['id'], $bb);

?>



<!DOCTYPE html>

<html>

<head>



    <!-- Basic -->

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <title>FORWARD</title>



    <meta name="keywords" content="Forward"/>

    <meta name="description" content="Aplicacion forward">

    <meta name="author" content="IDRA">



    <!-- Favicon -->

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">



    <!-- Mobile Metas -->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">



    <!-- Web Fonts  -->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">



    <!-- Vendor CSS -->

    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css?<?php echo time()?>">

    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" href="vendor/animate/animate.min.css">

    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">

    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">

    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">

    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">



    <!-- Theme CSS -->

    <link rel="stylesheet" href="css/theme.css?<?php echo time()?>">

    <link rel="stylesheet" href="css/theme-elements.css">

    <link rel="stylesheet" href="css/theme-blog.css">

    <link rel="stylesheet" href="css/theme-shop.css">



    <!-- Demo CSS -->





    <!-- Skin CSS -->

    <link rel="stylesheet" href="css/skins/default.css?<?php echo time()?>">



    <!-- Theme Custom CSS -->

    <link rel="stylesheet" href="css/custom.css">



    <!-- Head Libs -->

    <script src="vendor/modernizr/modernizr.min.js"></script>



</head>

<body>



<div class="body">

    <?php include 'sidebar.php' ?>



    <div role="main" class="main">



        <section class="page-header">

            <div class="container">

                <div class="row">

                    <div class="col">

                        <h1>Datos del encuentro/partido</h1>

                    </div>

                </div>

            </div>

        </section>



        <div class="container">



            <div class="featured-boxes">

                <div class="row">

                    <div class="col-lg-6 col-sm-6">

                        <div class="featured-box featured-box-primary featured-box-effect-1 mt-0 mt-lg-5">

                            <div class="box-content">

                                <i class="icon-featured fa fa-soccer-ball-o"></i>

                                <p>Torneo <?php echo $name ?></p>

                                <p><?php echo $match['hour_game_fixture'] ?></p>



                                <table width="100%">

                                    <tr>

                                        <td colspan="3">

                                            <h4 class="text-uppercase" style="color: black"><?php echo $bb1n . ' [' . $List1. ']' ?></h4>

                                            <h4 class="text-uppercase" style="color: black"><?php echo $bbn . ' [' . $List2 . ']' ?></h4>

                                        </td>

                                    </tr>



                                   
                                    <tr>

                                        <td><b>GOL(ES)</b></td>

                                        <td>

                                            <?php
                                            
                                                foreach ($goalScorerList as $goal_scorer){
                                                    
                                                    if(empty($goal_scorer['name_jugador'])){  
                                                        echo $goal_scorer['minute_goal_scorer'] ."' ". $goal_scorer['name_player'] .'<br>';
                                                      }else{
                
                                                        echo $goal_scorer['minute_goal_scorer'] ."' ". $goal_scorer['name_jugador'] .'<br>';
                                                        
                                                        }
                                                        


                                                }
                                               
                                             
                                        
                        
                                            ?>

                                        </td>

                                      

                                    </tr>

                                    <tr>

                                        <td><b>AMONESTACION(ES)</b></td>

                                        <td>

                                            <?php

                                            foreach ($cautions1 as $caution)

                                                echo $caution['minute_cautioned_player'] ."' ". $caution['name_player'] .'<br>';

                                            ?>

                                        </td>

                                        <td>

                                            <?php

                                            foreach ($cautions2 as $caution)

                                                echo $caution['minute_cautioned_player'] ."' ". $caution['name_player'] .'<br>';

                                            ?>

                                        </td>

                                    </tr>

                                </table>



                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



    <?php include 'footer.php' ?>

</div>



<!-- Vendor -->

<script src="vendor/jquery/jquery.min.js"></script>

<script src="vendor/jquery.appear/jquery.appear.min.js"></script>

<script src="vendor/jquery.easing/jquery.easing.min.js"></script>

<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>

<script src="vendor/popper/umd/popper.min.js"></script>

<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<script src="vendor/common/common.min.js"></script>

<script src="vendor/jquery.validation/jquery.validation.min.js"></script>

<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>

<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>

<script src="vendor/isotope/jquery.isotope.min.js"></script>

<script src="vendor/owl.carousel/owl.carousel.min.js"></script>

<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<script src="vendor/vide/vide.min.js"></script>



<!-- Theme Base, Components and Settings -->

<script src="js/theme.js"></script>



<!-- Theme Custom -->

<script src="js/custom.js"></script>



<!-- Theme Initialization Files -->

<script src="js/theme.init.js"></script>



<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.

<script>

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');



    ga('create', 'UA-12345678-1', 'auto');

    ga('send', 'pageview');

</script>

 -->



</body>

</html>

