<?php
date_default_timezone_set('America/La_Paz');
include_once 'admin/connection.php';
include_once 'admin/models/model_ranking_anual.php';

$ranking_anualModel= new Ranking_Anual_Model();

$rankingList = $ranking_anualModel->getAll();




/*
if($rankingList){
    foreach ($rankingList AS $id => $info){
        $rankingList['id_ranking']=$info['id_ranking'];
        $rankingList['genero'] = $info['genero'];
        $rankingList['id_sport'] = $info['id_sport'];
        $rankingList['id_category'] = $info['id_category'];
        $rankingList['id_player']=$info['id_player'];
        $rankingList['posicion'] = $info['posicion'];
        $rankingList['record']=$info['record'];
        $rankingList['detalle'] = $info['detalle'];
      
       
    }
}
*/


//if (!$rankingList){
  //  header('Location: index.php');//
//}

?>



<!DOCTYPE html>
<html>
<head>
    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ranking Anual</title>



    <meta name="keywords" content="Forward"/>

    <meta name="description" content="Aplicacion forward">

    <meta name="author" content="IDRA">



    <!-- Favicon -->

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">



    <!-- Mobile Metas -->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">



    <!-- Web Fonts  -->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">



    <!-- Vendor CSS -->

    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css?<?php echo time() ?>">

    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" href="vendor/animate/animate.min.css">

    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">

    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">

    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">

    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">



    <!-- Theme CSS -->

    <link rel="stylesheet" href="css/theme.css?<?php echo time() ?>">

    <link rel="stylesheet" href="css/theme-elements.css">

    <link rel="stylesheet" href="css/theme-blog.css">

    <link rel="stylesheet" href="css/theme-shop.css">



    <link rel="stylesheet" href="admin/plugins/lightslider/css/lightslider.css">



    <style>

        table td {

            border-top: 1px #CAD41E dashed;

        }



        .slider_panel {

            width: 100%;

            background-color: #000000;

            padding: 10px;

        }



        .slider_panel ul {

            list-style: none outside none;

            padding-left: 0;

            margin-bottom: 0;

        }



        .slider_panel ul li {

            display: block;

            float: left;

            margin-right: 6px;

            cursor: pointer;

        }



        .slider_panel ul li img {

            display: block;

            height: auto;

            max-width: 100%;

        }



        .lSSlideOuter .lSPager.lSGallery img {

            max-height: 80px;

        }

    </style>





    <!-- Demo CSS -->





    <!-- Skin CSS -->

    <link rel="stylesheet" href="css/skins/default.css?<?php echo time() ?>">



    <!-- Theme Custom CSS -->

    <link rel="stylesheet" href="css/custom.css">



    <!-- Head Libs -->

    <script src="vendor/modernizr/modernizr.min.js"></script>



    <script src="vendor/jquery/jquery.min.js"></script>





</head>

<body>



<div class="body">

    <?php include 'sidebar.php' ?>



    <div role="main" class="main">



        <section class="page-header">

            <div class="container">

                <div class="row">

                    <div class="col">

                        <h1>Ranking  Anual</h1>

                    </div>

                </div>

            </div>

        </section>



        <section class="section section-default-scale-lighten m-0">

            <div class="container-fluid sample-item-container">


            <?php  if (empty($rankingList)){
                
                echo '<h3>NO HAY RANKING ANUAL DISPONIBLE EN ESTE MOMENTO!</h3>';
                ?>
                       
                  <?php  }else{
                      
                      

                      
                      ?>


<div class="row text-center">
                    <div class="col-lg-12 pb-1">
                        <h4><b>LISTA DE RANKING ANUAL</b></h4>
                    </div>
                </div>


                <hr>


                <div class="row">
                    <div class="col">
                        <section class="call-to-action featured featured-primary button-centered" style="">
                            <div class="col-12">
                                <div class="call-to-action-content">
                                    <h4 class="text-uppercase"><strong>RANKING ANUAL</strong></h4>
                                        <br>
                                    <p class="mb-0">

                                    <table width="100%">

                                        <tr>

                                            <th>FOTO</th>

                                            <th>NOMBRE</th>

                                            <th>DEPORTE</th>

                                            <th>GENERO</th>

                                            <th>CATEGORIA</th>

                                            <th>RECORD</th>
                                            <th>POSICION</th>
                                            <th>DETALLE</th>

                                          

                                        </tr>

                                      
                                       
                                        <?php

                                        foreach ($rankingList as $player)

                                        { ?>
                                          
                                            <tr>

                                                <td>
                                                   <br>
                                                    <?php
                                                      
                                                  
                                                    $pathPhoto = 'admin/img/team/';
                                                    $pathPhoto1 = 'admin/img/player/';
                                                 
                                                 
                                                    if(empty($player['id_player'])){
                                                        $photoPlayer = $pathPhoto .$player['id_team'] . '.png';
                                                       
                                                    }else{
                                                        $photoPlayer = $pathPhoto1 .$player['id_player'] . '.jpg';
                                                    }
                                                    if (file_exists($photoPlayer))
                                                    
                                                    { ?>
                                                    
                                                        <img src="<?php echo $photoPlayer . '?' . time() ?>" style=" max-height: 150px;border-radius: 50%; height:80px;
                            width: 80px;
                            border: 2px solid;
                            border-color: #46595D;     margin-bottom: 15px;"> 
                                                    <?php } else{ ?>
                                                        
                                                        <img src="admin/img/icons/inco.png" style=" max-height: 150px;border-radius: 50%; height:80px;
                            width: 80px;
                            border: 2px solid;
                            border-color: #46595D;     margin-bottom: 15px;">
                            
                                                    <?php } ?>
                                                    
                                                </td>

                                                <td>
                                                
                                                    <?php 
                                                if(empty($player['name_player'])){
                                                    echo $player['name_team'];
                                                }else{
                                                    echo $player['name_player'];
                                                }
                                                
                                                ?></td>

                                                <td>
                                                    <?php echo $player['name_sport'] ?></td>

                                                <td> <?php echo $player['genero'] ?></td>

                                                <td><?php echo $player['name_category'] ?></td>

                                                <td><?php echo $player['record'] ?></td>
                                                <td><?php echo $player['posicion'] ?></td>
                                                <td><?php echo $player['detalle'] ?></td>

                                            </tr>

                                        <?php }

                                        ?>



                                    </table>

                                    </p>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>
                  <?php }?>


                


                <hr>


            </div>

        </section>





    </div>



    <?php include 'footer.php' ?>

</div>



<!-- Vendor -->

<script src="vendor/jquery/jquery.min.js"></script>

<script src="vendor/jquery.appear/jquery.appear.min.js"></script>

<script src="vendor/jquery.easing/jquery.easing.min.js"></script>

<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>

<script src="vendor/popper/umd/popper.min.js"></script>

<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<script src="vendor/common/common.min.js"></script>

<script src="vendor/jquery.validation/jquery.validation.min.js"></script>

<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>

<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>

<script src="vendor/isotope/jquery.isotope.min.js"></script>

<script src="vendor/owl.carousel/owl.carousel.min.js"></script>

<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<script src="vendor/vide/vide.min.js"></script>



<!-- Theme Base, Components and Settings -->

<script src="js/theme.js"></script>



<!-- Theme Custom -->

<script src="js/custom.js"></script>



<!-- Theme Initialization Files -->

<script src="js/theme.init.js"></script>



<script src="admin/plugins/lightslider/js/lightslider.js"></script>



<script>



    $(document).ready(function () {

        $(".lightSlider").lightSlider({

            gallery: true,

            item: 1,

            loop: true,

            slideMargin: 0,

            adaptiveHeight: true,

            responsive: [

                {

                    breakpoint: 800,

                    settings: {

                        thumbItem: 3

                    }

                },

                {

                    breakpoint: 480,

                    settings: {

                        thumbItem: 2

                    }

                }

            ]

        });

    });



</script>



</body>

</html>

