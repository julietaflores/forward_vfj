<?php
date_default_timezone_set('America/La_Paz');
include_once 'admin/connection.php';
include_once 'admin/models/model_ranking_anual.php';
include_once 'admin/models/model_event.php';
include_once 'admin/models/model_tournament.php';
include_once 'admin/models/model_sport.php';
include_once 'admin/models/model_category.php';
$sTournamentModel = new Tournament_Model();
$sTournamentsList = $sTournamentModel->getAll();
$ranking_anualModel= new Ranking_Anual_Model();
$rankingList = $ranking_anualModel->getAll();

$sportModel = new Sport_Model();
$sportList = $sportModel->getAll();

?>



<!DOCTYPE html>
<html>
<head>
    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ranking Anual</title>
    <meta name="keywords" content="Forward"/>
    <meta name="description" content="Aplicacion forward">
    <meta name="author" content="IDRA">
    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css?<?php echo time() ?>">
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendor/animate/animate.min.css">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="css/theme.css?<?php echo time() ?>">
    <link rel="stylesheet" href="css/theme-elements.css">
    <link rel="stylesheet" href="css/theme-blog.css">
    <link rel="stylesheet" href="css/theme-shop.css">
    <link rel="stylesheet" href="admin/plugins/lightslider/css/lightslider.css">

    <style>
        table td {
            border-top: 1px #CAD41E dashed;
        }

        .slider_panel {
            width: 100%;
            background-color: #000000;
            padding: 10px;
        }



        .slider_panel ul {
            list-style: none outside none;
            padding-left: 0;
            margin-bottom: 0;
        }



        .slider_panel ul li {
            display: block;
            float: left;
            margin-right: 6px;
            cursor: pointer;
        }

        .slider_panel ul li img {
            display: block;
            height: auto;
            max-width: 100%;
        }

        .lSSlideOuter .lSPager.lSGallery img {
            max-height: 80px;
        }

    </style>


    <!-- Demo CSS -->





    <!-- Skin CSS -->

    <link rel="stylesheet" href="css/skins/default.css?<?php echo time() ?>">
    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Head Libs -->

    <script src="vendor/modernizr/modernizr.min.js"></script>
    <script src="vendor/jquery/jquery.min.js"></script>



    <script>






       function asignarg(name){
        $('.teams_panel_original').hide(2000); 
        document.getElementById("valorg").innerHTML = name;
        var number=document.getElementById("userMsg").value;  
        document.getElementById("userMsgg").value = name;
         if(number!='' ){
              var numberc=document.getElementById("userMsgc").value; 
              if(numberc!='' ){

                  if(name=="TODOS"){
                    $.ajax({
            data: {
                idSport:number,
                idCategory:numberc
            },
            url: 'https://admin.forwardmundial.com/admin/ajax/list_sport_categoria.php',
            type: 'post',
            async: false,
            success: function (data) {
                if (data.length > 2) {   
                            $('.mens').hide(500);
                            $("#tablaReportes1 tr").remove(); 
                            $('.tablau').hide();
                            $('.teams_panel_original1').show(1500);
                            $.each($.parseJSON(data), function(idx, obj) {
                                if(obj.id_player !=''){
                                    imagen=   "<img src='admin/img/team/"+obj.id_player+".png'"+"style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";  
								}else{
                                    imagen=   "<img src='admin/img/icons/inco.png' style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";
                                }
                                var fila="<tr><td><br>"+ imagen+"</td> <td>"+obj.name_team+"</td> <td>"+obj.name_sport+"</td> <td>"+obj.name_category+"</td> <td>"+obj.genero+"</td> <td>"+obj.record+"</td> <td>"+obj.posicion+"</td> <td>"+obj.detalle+"</td>   </tr>";
                                var btn = document.createElement("TR");
                                btn.innerHTML=fila;
                                document.getElementById("tablaReportes1").appendChild(btn);   
                            });
                              $('.tablau').show(4000);
                        }else{ 
                            $('.teams_panel_original1').hide(1000); 
                            $('.mens').hide(800);
                            $('.tablau').hide(500); 
                            $('.mens').show(1500); 
                        } 
                }
            });



                  }else{

                    $.ajax({
                    data: {
                        idGenero: name,
                        idSport:number,
                        idCategory:numberc
                    },
                    url: 'https://admin.forwardmundial.com/admin/ajax/list_genero_sport_category.php',
                    type: 'post',
                    async: false,
                    success: function (data) {
                        if (data.length > 2) {   
                            $('.mens').hide(500);
                            $("#tablaReportes1 tr").remove(); 
                            $('.tablau').hide();
                            $('.teams_panel_original1').show(1500);
                            $.each($.parseJSON(data), function(idx, obj) {
                                if(obj.id_player !=''){
                                    imagen=   "<img src='admin/img/team/"+obj.id_player+".png'"+"style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";  
								}else{
                                    imagen=   "<img src='admin/img/icons/inco.png' style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";
                                }
                                var fila="<tr><td><br>"+ imagen+"</td> <td>"+obj.name_team+"</td> <td>"+obj.name_sport+"</td> <td>"+obj.name_category+"</td> <td>"+obj.genero+"</td> <td>"+obj.record+"</td> <td>"+obj.posicion+"</td> <td>"+obj.detalle+"</td>   </tr>";
                                var btn = document.createElement("TR");
                                btn.innerHTML=fila;
                                document.getElementById("tablaReportes1").appendChild(btn);   
                            });
                              $('.tablau').show(4000);
                        }else{ 
                            $('.teams_panel_original1').hide(1000); 
                            $('.mens').hide(800);
                            $('.tablau').hide(500); 
                            $('.mens').show(1500); 
                        } 
                     }
                   });

                  }


                

              }else{

                 if(name=="TODOS"){
                    $.ajax({
                    data: {
                        idSport: number
                    },
                    url: 'https://admin.forwardmundial.com/admin/ajax/list_sport.php',
                    type: 'post',
                    async: false,
                    success: function (data) {
                        if (data.length > 2) {   
                            $('.mens').hide(500);
                            $("#tablaReportes1 tr").remove(); 
                            $('.tablau').hide();
                            $('.teams_panel_original1').show(1500);
                            $.each($.parseJSON(data), function(idx, obj) {
                                if(obj.id_player !=''){
                                    imagen=   "<img src='admin/img/team/"+obj.id_player+".png'"+"style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";  
								}else{
                                    imagen=   "<img src='admin/img/icons/inco.png' style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";
                                }
                                var fila="<tr><td><br>"+ imagen+"</td> <td>"+obj.name_team+"</td> <td>"+obj.name_sport+"</td> <td>"+obj.name_category+"</td> <td>"+obj.genero+"</td> <td>"+obj.record+"</td> <td>"+obj.posicion+"</td> <td>"+obj.detalle+"</td>   </tr>";
                                var btn = document.createElement("TR");
                                btn.innerHTML=fila;
                                document.getElementById("tablaReportes1").appendChild(btn);   
                            });
                              $('.tablau').show(4000);
                        }else{ 
                            $('.teams_panel_original1').hide(1000); 
                            $('.mens').hide(800);
                            $('.tablau').hide(500); 
                            $('.mens').show(1500); 
                        } 
                    }
                   });
                 }else{

                    $.ajax({
                    data: {
                        idGenero: name,
                        idSport:number
                    },
                    url: 'https://admin.forwardmundial.com/admin/ajax/list_genero_sport.php',
                    type: 'post',
                    async: false,
                    success: function (data) {
                        if (data.length > 2) {   
                            $('.mens').hide(500);
                            $("#tablaReportes1 tr").remove(); 
                            $('.tablau').hide();
                            $('.teams_panel_original1').show(1500);
                            $.each($.parseJSON(data), function(idx, obj) {
                                if(obj.id_player !=''){
                                    imagen=   "<img src='admin/img/team/"+obj.id_player+".png'"+"style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";  
								}else{
                                    imagen=   "<img src='admin/img/icons/inco.png' style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";
                                }
                                var fila="<tr><td><br>"+ imagen+"</td> <td>"+obj.name_team+"</td> <td>"+obj.name_sport+"</td> <td>"+obj.name_category+"</td> <td>"+obj.genero+"</td> <td>"+obj.record+"</td> <td>"+obj.posicion+"</td> <td>"+obj.detalle+"</td>   </tr>";
                                var btn = document.createElement("TR");
                                btn.innerHTML=fila;
                                document.getElementById("tablaReportes1").appendChild(btn);   
                            });
                              $('.tablau').show(4000);
                        }else{ 
                            $('.teams_panel_original1').hide(1000); 
                            $('.mens').hide(800);
                            $('.tablau').hide(500); 
                            $('.mens').show(1500); 
                        } 
                     }
                   });

                 }


             


              }
     

         }else{
              if(name=="TODOS"){
                  alert("Hola seleccione DAMAS o VARONES  si quiere ver la lista determinada !!! => ");
                  $.ajax({
                    url: 'https://admin.forwardmundial.com/admin/ajax/list_genero_todos.php',
                    async: false,
                    success: function (data) {
                        if (data.length > 2) {   
                            $('.mens').hide(500);
                            $("#tablaReportes1 tr").remove(); 
                            $('.tablau').hide();
                            $('.teams_panel_original1').show(1500);
                            $.each($.parseJSON(data), function(idx, obj) {
                                if(obj.id_player !=''){
                                    imagen=   "<img src='admin/img/team/"+obj.id_player+".png'"+"style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";  
								}else{
                                    imagen=   "<img src='admin/img/icons/inco.png' style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";
                                }
                                var fila="<tr><td><br>"+ imagen+"</td> <td>"+obj.name_team+"</td> <td>"+obj.name_sport+"</td> <td>"+obj.name_category+"</td> <td>"+obj.genero+"</td> <td>"+obj.record+"</td> <td>"+obj.posicion+"</td> <td>"+obj.detalle+"</td>   </tr>";
                                var btn = document.createElement("TR");
                                btn.innerHTML=fila;
                                document.getElementById("tablaReportes1").appendChild(btn);   
                            });
                              $('.tablau').show(4000);
                        }else{ 
                            $('.teams_panel_original1').hide(1000); 
                            $('.mens').hide(800);
                            $('.tablau').hide(500); 
                            $('.mens').show(1500); 
                        } 
                     }
                   });

              }else{
                $.ajax({
                    data: {
                        idGenero: name
                    },
                    url: 'https://admin.forwardmundial.com/admin/ajax/list_genero.php',
                    type: 'post',
                    async: false,
                    success: function (data) {
                        if (data.length > 2) {   
                            $('.mens').hide(500);
                            $("#tablaReportes1 tr").remove(); 
                            $('.tablau').hide();
                            $('.teams_panel_original1').show(1500);
                            $.each($.parseJSON(data), function(idx, obj) {
                                if(obj.id_player !=''){
                                    imagen=   "<img src='admin/img/team/"+obj.id_player+".png'"+"style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";  
								}else{
                                    imagen=   "<img src='admin/img/icons/inco.png' style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";
                                }
                                var fila="<tr><td><br>"+ imagen+"</td> <td>"+obj.name_team+"</td> <td>"+obj.name_sport+"</td> <td>"+obj.name_category+"</td> <td>"+obj.genero+"</td> <td>"+obj.record+"</td> <td>"+obj.posicion+"</td> <td>"+obj.detalle+"</td>   </tr>";
                                var btn = document.createElement("TR");
                                btn.innerHTML=fila;
                                document.getElementById("tablaReportes1").appendChild(btn);   
                            });
                              $('.tablau').show(4000);
                        }else{ 
                            $('.teams_panel_original1').hide(1000); 
                            $('.mens').hide(800);
                            $('.tablau').hide(500); 
                            $('.mens').show(1500); 
                        } 
                     }
                   });
              } 
         }
       }


       function asignar(id,name){
        $('.teams_panel_original').hide(2000); 
        document.getElementById("valor").innerHTML = name;
        document.getElementById("userMsg").value = id;
        var genero = document.getElementById("userMsgg").value;
        $("#id_category1 ul").remove();
        document.getElementById("valor1").innerHTML = "";
        $('#userMsgc').val('');
        $.ajax({
            data: {
              idSport: id,
              selectedId: ''
            },
           url: 'https://admin.forwardmundial.com/admin/ajax/html_categories_by_sport_ul.php',
           type: 'post',
           success: function (html_select_response) {
              $('#id_category1').html(html_select_response);
           }
         });

               if(genero!=''){
                 if(genero=="TODOS"){
                    $.ajax({
                    data: {
                        idSport: id
                    },
                    url: 'https://admin.forwardmundial.com/admin/ajax/list_sport.php',
                    type: 'post',
                    async: false,
                    success: function (data) {
                        if (data.length > 2) {   
                            $('.mens').hide(500);
                            $("#tablaReportes1 tr").remove(); 
                            $('.tablau').hide();
                            $('.teams_panel_original1').show(1500);
                            $.each($.parseJSON(data), function(idx, obj) {
                                if(obj.id_player !=''){
                                    imagen=   "<img src='admin/img/team/"+obj.id_player+".png'"+"style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";  
								}else{
                                    imagen=   "<img src='admin/img/icons/inco.png' style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";
                                }
                                var fila="<tr><td><br>"+ imagen+"</td> <td>"+obj.name_team+"</td> <td>"+obj.name_sport+"</td> <td>"+obj.name_category+"</td> <td>"+obj.genero+"</td> <td>"+obj.record+"</td> <td>"+obj.posicion+"</td> <td>"+obj.detalle+"</td>   </tr>";
                                var btn = document.createElement("TR");
                                btn.innerHTML=fila;
                                document.getElementById("tablaReportes1").appendChild(btn);   
                            });
                              $('.tablau').show(4000);
                        }else{ 
                            $('.teams_panel_original1').hide(1000); 
                            $('.mens').hide(800);
                            $('.tablau').hide(500); 
                            $('.mens').show(1500); 
                        }  
                    }
                   });


                 }else{


                    $.ajax({
                    data: {
                        idGenero: genero,
                        idSport:id
                    },
                    url: 'https://admin.forwardmundial.com/admin/ajax/list_genero_sport.php',
                    type: 'post',
                    async: false,
                    success: function (data) {
                        if (data.length > 2) {   
                            $('.mens').hide(500);
                            $("#tablaReportes1 tr").remove(); 
                            $('.tablau').hide();
                            $('.teams_panel_original1').show(1500);
                            $.each($.parseJSON(data), function(idx, obj) {
                                if(obj.id_player !=''){
                                    imagen=   "<img src='admin/img/team/"+obj.id_player+".png'"+"style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";  
								}else{
                                    imagen=   "<img src='admin/img/icons/inco.png' style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";
                                }
                                var fila="<tr><td><br>"+ imagen+"</td> <td>"+obj.name_team+"</td> <td>"+obj.name_sport+"</td> <td>"+obj.name_category+"</td> <td>"+obj.genero+"</td> <td>"+obj.record+"</td> <td>"+obj.posicion+"</td> <td>"+obj.detalle+"</td>   </tr>";
                                var btn = document.createElement("TR");
                                btn.innerHTML=fila;
                                document.getElementById("tablaReportes1").appendChild(btn);   
                            });
                              $('.tablau').show(4000);
                        }else{ 
                            $('.teams_panel_original1').hide(1000); 
                            $('.mens').hide(800);
                            $('.tablau').hide(500); 
                            $('.mens').show(1500); 
                        } 
                     }
                   });
         

                 }



               }else{

                $.ajax({
                    data: {
                        idSport: id
                    },
                    url: 'https://admin.forwardmundial.com/admin/ajax/list_sport.php',
                    type: 'post',
                    async: false,
                    success: function (data) {
                      
                        if (data.length > 2) {   
                            $('.mens').hide(500);
                            $("#tablaReportes1 tr").remove(); 
                            $('.tablau').hide();
                            $('.teams_panel_original1').show(1500);
                            $.each($.parseJSON(data), function(idx, obj) {
                                if(obj.id_player !=''){
                                    imagen=   "<img src='admin/img/team/"+obj.id_player+".png'"+"style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";  
								}else{
                                    imagen=   "<img src='admin/img/icons/inco.png' style='max-height: 150px;border-radius: 50%; height:80px;width: 80px;border: 2px solid;  border-color: #46595D;     margin-bottom: 15px;'>";
                                }
                                var fila="<tr><td><br>"+ imagen+"</td> <td>"+obj.name_team+"</td> <td>"+obj.name_sport+"</td> <td>"+obj.name_category+"</td> <td>"+obj.genero+"</td> <td>"+obj.record+"</td> <td>"+obj.posicion+"</td> <td>"+obj.detalle+"</td>   </tr>";
                                var btn = document.createElement("TR");
                                btn.innerHTML=fila;
                                document.getElementById("tablaReportes1").appendChild(btn);   
                            });
                              $('.tablau').show(4000);
                        }else{ 
                            $('.teams_panel_original1').hide(1000); 
                            $('.mens').hide(800);
                            $('.tablau').hide(500); 
                            $('.mens').show(1700); 
                        } 
                    }
                   });



               }
 

              




      

       
       
       }
    </script>

</head>

<body>



<div class="body">

    <?php include 'sidebar.php' ?>



    <div role="main" class="main">



        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h1>Ranking  Anual</h1>
                    </div>
                </div>
            </div>
        </section>



        <section class="section section-default-scale-lighten m-0" style="    padding-top: 0px;">
            <div class="container-fluid sample-item-container">

            <?php  if (empty($rankingList)){    echo '<h3>NO HAY RANKING ANUAL DISPONIBLE EN ESTE MOMENTO!</h3>'; ?>
                       
            <?php  }else{ ?>

                <div class="row">
                    <div class="col">
                        <section class="call-to-action featured featured-primary button-centered" >
                            <div class="col-12">
                                <div class="call-to-action-content" style="padding-top: 0px;">
                        
                               
                                        <header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 55, 'stickySetTop': '-55px', 'stickyChangeLogo': true}">
<div class="header">
    <div class="header-container container">
        <div class="header-row">
            <div class="header-column justify-content-end">
                <div class="header-row">
                    <div class="header-nav">
                        <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">
                            <nav class="collapse">
                                <ul class="nav nav-pills" id="mainNav">
                                  

                                    <li class="dropdown">
                                        <a style="font-size: 15px;" class="dropdown-item dropdown-toggle" >
                                            Deportes
                                        </a>
                                       <ul class="dropdown-menu" >
                                            <?php foreach ($sportList as $sEvent){ ?>
                                            <li>
                                                <a  id="id_sport" name="id_sport" onclick="asignar('<?php echo $sEvent['id_sport'] ?>','<?php echo $sEvent['name_sport'] ?>');"  class="dropdown-item" ><?php echo $sEvent['name_sport'] ?></a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                  <strong>      <label  style="visibility: none; font-size: 15px; " id="valor" name="valor" > </label></strong>
                                        <input type="hidden" id="userMsg" name="userMsg"/>

                                    </li>
                                 
                                    <li class="dropdown" >

                                        <a style="font-size: 15px;" class="dropdown-item dropdown-toggle" >
                                            Categoria
                                        </a>

                                        <ul class="dropdown-menu" id="id_category1" name="id_category1"> 
                                        </ul>

                                       <strong> <label style="visibility: none; font-size: 15px;" id="valor1" name="valor1" > </label></strong>
                                        <input type="hidden" id="userMsgc" name="userMsgc"/>


                                    </li>


                                    <li class="dropdown">
                                        <a style="font-size: 15px;" class="dropdown-item dropdown-toggle" >
                                            Genero
                                        </a>
                                        <ul class="dropdown-menu" >
                                           
                                            <li>
                                                <a    onclick="asignarg('VARONES');"  class="dropdown-item" >VARONES</a>
                                            </li>
                                         
                                            <li>
                                                <a    onclick="asignarg('DAMAS');"  class="dropdown-item" >DAMAS</a>
                                            </li>


                                            <li>
                                                <a   onclick="asignarg('TODOS');"  class="dropdown-item" >TODOS</a>
                                            </li>

                                        </ul>
                                        <strong><label  style="visibility: none; font-size: 15px;" id="valorg" name="valorg" > </label></strong>
                                        <input type="hidden" id="userMsgg" name="userMsgg" />

                                    </li>

                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</header>



<h3  style="display: none;  font: oblique bold 180% cursive;" class="mens">NO HAY LISTA DE RANKING ANUAL DISPONIBLE EN ESTE MOMENTO !!!</h3>       
                                        
                                    <p class="mb-0">

                                    <table class="menuu" width="100%">

                                        <tr class="teams_panel_original1"  style="font-size: larger;">

                                            <th>FOTO</th>
                                            <th>NOMBRE</th>
                                            <th>DEPORTE</th>
                                            <th>CATEGORIA</th>
                                            <th>GENERO</th>
                                            <th>LUGAR</th>
                                            <th>POSICION</th>
                                            <th>DETALLE</th>

                                          

                                        </tr>
                                     

                                        <tbody class="teams_panel_original">   
                                       
                                        <?php

                                        foreach ($rankingList as $player)

                                        { ?>
                                          
                                            <tr >

                                                <td>
                                                   <br>
                                                    <?php
                                                      
                                                  
                                                    $pathPhoto = 'admin/img/team/';
                                                    $pathPhoto1 = 'admin/img/player/';
                                                 
                                                 
                                                    if(!empty($player['id_player'])){
                                                        $photoPlayer = $pathPhoto .$player['id_player'].'.png';
                                                       
                                                    }else{
                                                        $photoPlayer = $pathPhoto1 .$player['id_player'].'.jpg';
                                                    }

                                                   // print_r($photoPlayer);
                                                    if (file_exists($photoPlayer))
                                                    { ?>
                                                    
                                                        <img src="<?php echo $photoPlayer . '?' . time() ?>" style=" max-height: 150px;border-radius: 50%; height:80px;
                            width: 80px;
                            border: 2px solid;
                            border-color: #46595D;     margin-bottom: 15px;"> 
                                                    <?php } else{ ?>
                                                        
                                                        <img src="admin/img/icons/inco.png" style=" max-height: 150px;border-radius: 50%; height:80px;
                            width: 80px;
                            border: 2px solid;
                            border-color: #46595D;     margin-bottom: 15px;">
                            
                                                    <?php } ?>
                                                    
                                                </td>

                                                <td>
                                                
                                                    <?php 
                                                if(empty($player['name_player'])){
                                                    echo $player['name_team'];
                                                }else{
                                                    echo $player['name_player'];
                                                }
                                                
                                                ?></td>

                                                <td><?php echo $player['name_sport'] ?></td>
                                                <td><?php echo $player['name_category'] ?></td>
                                                <td> <?php echo $player['genero'] ?></td>
                                                <td><?php echo $player['record'] ?></td>
                                                <td><?php echo $player['posicion'] ?></td>
                                                <td><?php echo $player['detalle'] ?></td>

                                            </tr>

                                        <?php }

                                        ?>

                                            </tbody>

                                     <tbody id="tablaReportes1" style="display: none;" class="tablau">   
                                     </tbody>

                                    </table>



                                   


                                    </p>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>
                  <?php }?>


                


                <hr>


            </div>

        </section>





    </div>



    <?php include 'footer.php' ?>

</div>



<!-- Vendor -->

<script src="vendor/jquery/jquery.min.js"></script>

<script src="vendor/jquery.appear/jquery.appear.min.js"></script>

<script src="vendor/jquery.easing/jquery.easing.min.js"></script>

<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>

<script src="vendor/popper/umd/popper.min.js"></script>

<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<script src="vendor/common/common.min.js"></script>

<script src="vendor/jquery.validation/jquery.validation.min.js"></script>

<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>

<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>

<script src="vendor/isotope/jquery.isotope.min.js"></script>

<script src="vendor/owl.carousel/owl.carousel.min.js"></script>

<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<script src="vendor/vide/vide.min.js"></script>



<!-- Theme Base, Components and Settings -->

<script src="js/theme.js"></script>



<!-- Theme Custom -->

<script src="js/custom.js"></script>



<!-- Theme Initialization Files -->

<script src="js/theme.init.js"></script>



<script src="admin/plugins/lightslider/js/lightslider.js"></script>



<script>



    $(document).ready(function () {

        $(".lightSlider").lightSlider({

            gallery: true,

            item: 1,

            loop: true,

            slideMargin: 0,

            adaptiveHeight: true,

            responsive: [

                {

                    breakpoint: 800,

                    settings: {

                        thumbItem: 3

                    }

                },

                {

                    breakpoint: 480,

                    settings: {

                        thumbItem: 2

                    }

                }

            ]

        });

    });



</script>



</body>

</html>

