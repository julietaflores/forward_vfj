<?php
if (!isset($_GET['id']))
    header('Location: index.php');

include_once 'admin/connection.php';
include_once 'admin/models/model_event.php';
include_once 'admin/models/model_tournament.php';

$eventModel = new Event_Model();
$event = $eventModel->getById( $_GET['id'] );


if($event){
    foreach ($event AS $id => $info){
        $event['id_event']=$info['id_event'];
        $event['name_event'] = $info['name_event'];
        $event['record'] = $info['record'];
    }
}




if( !$event ){
    header('Location: index.php');
}
  

$tournamentModel = new Tournament_Model();
$tournamentList = $tournamentModel->getAllByEvent($_GET['id']);
/*
if($tournamentList){
    foreach ($tournamentList AS $id => $info){
        $tournamentList['id_tournament']=$info['id_tournament'];
        $tournamentList['id_event'] = $info['id_event'];
        $tournamentList['id_sport'] = $info['id_sport'];
        $tournamentList['id_category'] = $info['id_category'];
        $tournamentList['date_tournament']=$info['date_tournament'];
        $tournamentList['name_tournament'] = $info['name_tournament'];
        $tournamentList['gender_tournament']=$info['gender_tournament'];
        $tournamentList['type_tournament'] = $info['type_tournament'];
        $tournamentList['number_series_tournament'] = $info['number_series_tournament'];
        $tournamentList['number_teams_by_serie_tournament'] = $info['number_teams_by_serie_tournament'];
        $tournamentList['schema_keys_tournament'] = $info['schema_keys_tournament'];
        $tournamentList['status_tournament'] = $info['status_tournament'];
        $tournamentList['tiempos'] = $info['tiempos'];
        $tournamentList['nsembrado'] = $info['nsembrado'];
        $tournamentList['tipot'] = $info['tipot'];
        $tournamentList['name_category'] = $info['name_category'];
        $tournamentList['name_sport'] = $info['name_sport'];
        $tournamentList['name_event'] = $info['name_event'];
       
    }
}
*/



?>

<!DOCTYPE html>
<html>
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>FORWARD</title>

    <meta name="keywords" content="Forward"/>
    <meta name="description" content="Aplicacion forward">
    <meta name="author" content="IDRA">

    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css?<?php echo time() ?>">
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendor/animate/animate.min.css">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="css/theme.css?<?php echo time() ?>">
    <link rel="stylesheet" href="css/theme-elements.css">
    <link rel="stylesheet" href="css/theme-blog.css">
    <link rel="stylesheet" href="css/theme-shop.css">

    <!-- Demo CSS -->


    <!-- Skin CSS -->
    <link rel="stylesheet" href="css/skins/default.css?<?php echo time() ?>">

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Head Libs -->
    <script src="vendor/modernizr/modernizr.min.js"></script>

    <script src="vendor/jquery/jquery.min.js"></script>
</head>
<body>

<div class="body">
    <?php include 'sidebar.php' ?>

    <div role="main" class="main">

        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h1>Torneos</h1>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-default-scale-lighten m-0">
            <div class="container-fluid sample-item-container">

                <div class="row text-center">
                    <div class="col-lg-12 pb-1">
                        <h4><b>TORNEOS DE EVENTO: <span style="font-style:italic"><?php echo $event['name_event']?></span></b></h4>
                    </div>
                </div>

                <div class="row sample-item-list sample-item-list-loaded" data-plugin-masonry data-plugin-options="{'itemSelector': '.isotope-item'}">
                    <?php
                    if (count($tournamentList) == 0){
                        echo '<h3>NO HAY TORNEOS DISPONIBLES EN ESTE MOMENTO!</h3>';
                    }
                       // print_r($tournamentList);
                       // exit();

                    foreach ($tournamentList AS $id => $info)
                    {
                        ?>
                        <div class="col-md-6 col-lg-4 isotope-item" align="center">
                            <div class="sample-item sample-item-home pl-3 pr-3">
                                <a href="tournament_details.php?id=<?php echo $info['id_tournament'] ?>">
                                <span class="sample-item-image-wrapper">
                                    <span class="sample-item-image">
                                       

                                    <?php
                                              $er="admin/img/sport/".$info['id_sport'].".jpeg";
                                              $err= $er.time();
                                             if(file_exists($er)){?>
<img src="admin/img/sport/<?php echo $info['id_sport'] ?>.jpeg?<?php echo time() ?>" height="180px">
                                            <?php }else{?>
                                                <img src="admin/img/logos/logo.jpg" height="180px">
                                            <?php }?>

                                        
                                    </span>
                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
								</span>
                                 <span class="sample-item-description">
                                    <h5 style="color: green"><?php echo $info['name_tournament'] ?></h5>
                                    <p>
                                         <b>GENERO</b>: <?php echo $info['gender_tournament'] ?>
                                         <br>
                                         <b>DEPORTE</b>: <?php echo $info['name_sport'] ?>
                                     </p>

                                </span>
                                </a>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>


    </div>

    <?php include 'footer.php' ?>
</div>

<!-- Vendor -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
<script src="vendor/popper/umd/popper.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/common/common.min.js"></script>
<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="vendor/isotope/jquery.isotope.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="vendor/vide/vide.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="js/theme.js"></script>

<!-- Theme Custom -->
<script src="js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="js/theme.init.js"></script>

<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-12345678-1', 'auto');
    ga('send', 'pageview');
</script>
 -->

</body>
</html>
