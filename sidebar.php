<?php

include_once 'admin/connection.php';

include_once 'admin/models/model_event.php';

include_once 'admin/models/model_tournament.php';



$sEventModel = new Event_Model();

$sEventsList = $sEventModel->getAll();



$sTournamentModel = new Tournament_Model();

$sTournamentsList = $sTournamentModel->getAll();



?>

<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 55, 'stickySetTop': '-55px', 'stickyChangeLogo': true}">

    <div class="header-body">

        <div class="header-container container">

            <div class="header-row">

                <div class="header-column">

                    <div class="header-row">

                        <div class="header-logo">

                            <a href="index.php">

                                <img alt="Porto" width="111" height="54" data-sticky-width="82" data-sticky-height="40" data-sticky-top="33" src="admin/img/logos/logo.jpg?<?php echo time()?>">

                            </a>

                        </div>

                    </div>

                </div>

                <div class="header-column justify-content-end">

                    <div class="header-row">

                        <div class="header-nav">

                            <div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1">

                                <nav class="collapse">

                                    <ul class="nav nav-pills" id="mainNav">

                                        <li class="">

                                            <a class="nav-link" href="index.php">

                                                Inicio

                                            </a>

                                        </li>

                                        <li class="">
                                               <a class="nav-link" href="admin/index.php">
                                                 Deportes
                                               </a>
    <!--                                        <a class="nav-link" href="deportes/index.php">

                                                Deportes

                                            </a>-->

                                        </li>

                                        <li class="dropdown">

                                            <a class="dropdown-item dropdown-toggle" href="#">

                                                Eventos

                                            </a>

                                            <ul class="dropdown-menu">

                                                <?php foreach ($sEventsList as $sEvent){ ?>

                                                <li>

                                                    <a class="dropdown-item" href="tournament.php?id=<?php echo $sEvent['id_event'] ?>"><?php echo $sEvent['name_event'] ?></a>

                                                </li>

                                                <?php } ?>

                                            </ul>

                                        </li>

                                        
                                        <li class="">

                                            <a class="nav-link" href="ranking_anual.php">

                                                RANKING ANUAL

                                            </a>

                                        </li>

                                        <li class="dropdown">

                                            <a class="dropdown-item dropdown-toggle" href="#">

                                                Torneos

                                            </a>

                                            <ul class="dropdown-menu">

                                                <?php foreach ($sTournamentsList as $sTournament){ ?>

                                                           

                                                    <li>

                                                    <?php if($sTournament['tipot'] == 3) { ?>

                                                         <?php if($sTournament['type_tournament'] == 'Llaves'){?>

                                                            <a class="dropdown-item" href="tournament_details_a_ll.php?id=<?php echo $sTournament['id_tournament'] ?>"><?php echo $sTournament['name_tournament'] ?></a>
                                                         <?php
                                                        }else{?>
   <a class="dropdown-item" href="tournament_detailsa.php?id=<?php echo $sTournament['id_tournament'] ?>"><?php echo $sTournament['name_tournament'] ?></a>
                                                      <?php
                                                        }?>

                                                     
                                                    <?php }else{ ?>

                                                        <a class="dropdown-item" href="tournament_details.php?id=<?php echo $sTournament['id_tournament'] ?>"><?php echo $sTournament['name_tournament'] ?></a>
                                                        
                                                    <?php } ?>


                                                    </li>

                                                <?php } ?>

                                            </ul>

                                        </li>

                                    </ul>

                                </nav>

                            </div>

                            <ul class="header-social-icons social-icons d-none d-sm-block">

                                <li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>

                            </ul>

                            <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">

                                <i class="fa fa-bars"></i>

                            </button>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</header>