<style>
    .aux{

    }

    .ads {
        padding: 0 10px;
        height: 100%;
        /*margin: auto;*/
        vertical-align: middle;
    }

    .ads img {
        border-radius: 10px;
        max-width: 200px;
        max-height: 100px;
    }
</style>


<div class="container">
    <div class="row">
        <div class="footer-ribbon" style="background-color: #CAD41E">
            <span>Auspiciados por:</span>
        </div>
    </div>

    <div class="row">
        <?php for ($i = 1; $i <= 10; $i++)
        {
            $ad = 'admin/img/banner/banner_' . $i . '.jpg';
            //echo $ad;
            if (is_file($ad))
            {
                ?>
                <div class="col-lg-3 col-md-6 aux" align="center">
                    <div class="ads">
                        <img src="<?php echo $ad . '?' . time() ?>">
                    </div>
                </div>
            <?php }
        } ?>
    </div>
</div>