<?php

use Mpdf\Tag\Li;

if (!isset($_GET['id'])){
    header('Location: index.php');
}

date_default_timezone_set('America/La_Paz');
include_once 'admin/connection.php';
include_once 'admin/models/model_tournament.php';
$tournamentModel = new Tournament_Model();
$tournament = $tournamentModel->getById($_GET['id']);


if($tournament){
    foreach ($tournament AS $id => $info){
        $tournament['id_tournament']=$info['id_tournament'];
        $tournament['id_event'] = $info['id_event'];
        $tournament['id_sport'] = $info['id_sport'];
        $tournament['id_category'] = $info['id_category'];
        $tournament['date_tournament']=$info['date_tournament'];
        $tournament['name_tournament'] = $info['name_tournament'];
        $tournament['gender_tournament']=$info['gender_tournament'];
        $tournament['type_tournament'] = $info['type_tournament'];
        $tournament['number_series_tournament'] = $info['number_series_tournament'];
        $tournament['number_teams_by_serie_tournament'] = $info['number_teams_by_serie_tournament'];
        $tournament['schema_keys_tournament'] = $info['schema_keys_tournament'];
        $tournament['status_tournament'] = $info['status_tournament'];
        $tournament['tiempos'] = $info['tiempos'];
        $tournament['nsembrado'] = $info['nsembrado'];
        $tournament['tipot'] = $info['tipot'];
        $tournament['name_category'] = $info['name_category'];
        $tournament['name_sport'] = $info['name_sport'];
        $tournament['name_event'] = $info['name_event'];
       
    }
}


if (!$tournament){
    header('Location: index.php');
}


include_once 'admin/models/model_tournament_teams.php';
include_once 'admin/models/model_fixture.php';
include_once 'admin/models/model_goal_scorer.php';
include_once('admin/models/model_cautioned_player.php');
include_once('admin/models/model_player.php');
include_once('admin/models/model_news.php');

//---------------------------------------------------

$tournamentTeamsModel = new Tournament_Teams_Model();

$tournamentTeamsList = $tournamentTeamsModel->getByTournament($_GET['id']);



//---------------------------------------------------

$fixtureImagesPath = "admin/img/fixture/" . $_GET['id'] . '/';



//---------------------------------------------------

$fixtureModel = new Fixture_Model();


$fixture=$fixtureModel->getAll11($_GET['id']);
$goalScorerModel = new Goal_Scorer_Model();










//---------------------------------------------------

$positionImagesPath = "admin/img/position/" . $_GET['id'] . '/';



//---------------------------------------------------


$scoresList = $goalScorerModel->getByTournamentTotalGoals1($_GET['id']);




//---------------------------------------------------

$cautionedPlayerModel = new Cautioned_Player_Model();

$cautionedList = $cautionedPlayerModel->getByTournamentTotalCations($_GET['id']);



//---------------------------------------------------


$list = $goalScorerModel->getByTournamentTotalGoalsaa($_GET['id']);

//---------------------------------------------------

$newsModel = new News_Model();

$newsList = $newsModel->getAllByTournament($tournament['id_tournament']);

?>



<!DOCTYPE html>

<html>

<head>



    <!-- Basic -->

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <title>FORWARD</title>



    <meta name="keywords" content="Forward"/>

    <meta name="description" content="Aplicacion forward">

    <meta name="author" content="IDRA">



    <!-- Favicon -->

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">



    <!-- Mobile Metas -->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">



    <!-- Web Fonts  -->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">



    <!-- Vendor CSS -->

    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css?<?php echo time() ?>">

    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" href="vendor/animate/animate.min.css">

    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">

    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">

    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">

    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">



    <!-- Theme CSS -->

    <link rel="stylesheet" href="css/theme.css?<?php echo time() ?>">

    <link rel="stylesheet" href="css/theme-elements.css">

    <link rel="stylesheet" href="css/theme-blog.css">

    <link rel="stylesheet" href="css/theme-shop.css">



    <link rel="stylesheet" href="admin/plugins/lightslider/css/lightslider.css">



    <style>

        table td {

            border-top: 1px #CAD41E dashed;

        }



        .slider_panel {

            width: 100%;

            background-color: #000000;

            padding: 10px;

        }



        .slider_panel ul {

            list-style: none outside none;

            padding-left: 0;

            margin-bottom: 0;

        }



        .slider_panel ul li {

            display: block;

            float: left;

            margin-right: 6px;

            cursor: pointer;

        }



        .slider_panel ul li img {

            display: block;

            height: auto;

            max-width: 100%;

        }



        .lSSlideOuter .lSPager.lSGallery img {

            max-height: 80px;

        }

    </style>





    <!-- Demo CSS -->





    <!-- Skin CSS -->

    <link rel="stylesheet" href="css/skins/default.css?<?php echo time() ?>">



    <!-- Theme Custom CSS -->

    <link rel="stylesheet" href="css/custom.css">



    <!-- Head Libs -->

    <script src="vendor/modernizr/modernizr.min.js"></script>



    <script src="vendor/jquery/jquery.min.js"></script>





</head>

<body>



<div class="body">

    <?php include 'sidebar.php' ?>



    <div role="main" class="main">



        <section class="page-header">

            <div class="container">

                <div class="row">

                    <div class="col">

                        <h1>Torneos</h1>

                    </div>

                </div>

            </div>

        </section>



        <section class="section section-default-scale-lighten m-0">

            <div class="container-fluid sample-item-container">



                <div class="row text-center">

                    <div class="col-lg-12 pb-1">

                        <h4><b>TORNEO: <span style="font-style:italic"><?php echo $tournament['name_tournament'] ?></span></b></h4>

                    </div>

                </div>



                <div class="row">

                    <div class="col">

                        <section class="call-to-action featured featured-primary button-centered" style="">

                            <div class="col-12">

                                <div class="call-to-action-content">

                                    <h4 class="text-uppercase"><strong>EQUIPOS</strong></h4>

                                    <p class="mb-0">

                                        <?php

                                        foreach ($tournamentTeamsList as $team)

                                        {

                                            echo '<p><a href="team.php?id=' . $team['id_team'] . '">' . $team['name_team'] . '</a></p>';

                                        }

                                        ?>

                                    </p>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>



                <hr>



                <div class="row">

                    <div class="col">

                        <section class="call-to-action featured featured-primary button-centered" style="">

                            <div class="col-12">

                                <div class="call-to-action-content">

                                    <h4 class="text-uppercase"><strong>SORTEO</strong></h4>

                                    <p class="mb-0">

                                    <div class="slider_panel">

                                        <ul class="lightSlider">

                                            <?php

                                            if (is_dir($fixtureImagesPath))

                                            {

                                                $filesList = array_slice(scandir($fixtureImagesPath), 2); // Without dots



                                                foreach ($filesList as $file)

                                                {

                                                    if (is_dir($fixtureImagesPath . $file))

                                                        echo '';

                                                    else if (strpos($file, '.jpg') !== false || strpos($file, '.png') !== false || strpos($file, '.jpeg') !== false)

                                                    {

                                                        ?>

                                                        <li data-thumb="<?php echo $fixtureImagesPath . $file ?>">

                                                            <img src="<?php echo $fixtureImagesPath . $file ?>"/>

                                                        </li>

                                                        <?php

                                                    }

                                                }

                                            }

                                            ?>

                                        </ul>

                                    </div>

                                    </p>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>



                <hr>



                <div class="row">

                    <div class="col">

                        <section class="call-to-action featured featured-primary button-centered" style="">

                            <div class="col-12">

                                <div class="call-to-action-content">

                                    <h4 class="text-uppercase"><strong>RESULTADOS</strong></h4>


<br>
                                    <table width="100%" class="table table-striped table-bordered">

                                        <tr>

                                            <th><strong>HORA</strong></th>

                                            <th style="text-align: center"><strong>EQUIPO</strong></th>

                                            <th></th>

                                            <th></th>

                                            <th></th>

                                            <th style="text-align: center"><strong>EQUIPO</strong></th>
                                            <th style="text-align: center"><strong>INFORMACION</strong></th>
                                        </tr>
                                    
                                        <?php

                                        foreach ($fixture as $info)
                                        {

                                            if(!empty($info['id_team_T1'])){
                                                $bb=$info['id_team_T1'];
                                                $bbn=$info['name_team_T1'];

                                            }else{
                                                $bb=$info['id_team_1'];
                                                $bbn=$info['t1Name'];
                                            }
                                            if(!empty($info['id_team_T2'])){
                                                $bb1=$info['id_team_T2'];
                                                $bb1n=$info['name_team_T2'];
                                            }else{
                                                $bb1=$info['id_team_2'];
                                                $bb1n=$info['t2Name'];
                                            }
                                    
                                            $List1 = $goalScorerModel->getAllai($_GET['id'], $info['id_fixture'],$bb1);
                                            $List2 = $goalScorerModel->getAllai($_GET['id'], $info['id_fixture'],$bb);
                                        
                                             if(empty($List1)){
                                                $List1=0;
                                             }else{
                                                 $List1=count($List1);
                                                 if(empty($List2)){
                                                     $List2=0;
                                                 }else{
                                                     $List2=count($List2);
                                                     if($List1<$List2){
                                                        $rt1=$bb1;
                                                        $rt2=$bb1n;
                                                        $rt3=$List1;
                                                        $bb1=$bb;
                                                        $bb1n=$bbn;
                                                        $List1=$List2;
                                                        $bb=$rt1;
                                                        $bbn=$rt2;
                                                        $List2=$rt3;

                                                     }
                                                 }
                                             }


                                            ?>

                                        <tr>

                                            <td><?php echo DateTime::createFromFormat('H:i:s', $info['hour_game_fixture'])->format('H:i') ?></td>

                                            <td align="center">
                                   
                

                              <?php
                                $aqui= "admin/img/team/".$bb1.".png";       
                                  if(file_exists($aqui)){
                                  ?>  
                                  <br>
                                <div class="user-panel" >
            <div class="pull-center image">
                <img src="admin/img/team/<?php echo  $bb1?>.png" class="img-circle" alt="User Image" style="border-radius: 50%;
                            height:80px;
                            width: 80px;
                            border: 2px solid;
                            border-color: #46595D;
                          ">
            </div>
            <br>
            <div class="pull-center info">
            <?php echo $bb1n  ?>
            </div>
        </div>                               
                               
                                  <?php }else{?>
                                    <br>
                                <div class="user-panel" >
            <div class="pull-center image">
                <img src="admin/img/icons/inco.png" class="img-circle" alt="User Image" style="border-radius: 50%;
                            height:80px;
                            width: 80px;
                            border: 2px solid;
                            border-color: #46595D;
                          ">
            </div>
            <br>
            <div class="pull-center info">
            <?php echo $bb1n ?>
            </div>
        </div>                             
                                <?php  }?>  

                                            
   
                                            
                                              
                                        
                                                
                                        
                                            </td>

                                            <td>
        

                                        
                                                <button type="button" class="btn btn-default" style="margin-top: 130px"> <?php echo $List1 ?> </button>          
                                           
                                        </td>

                                            <td align="center" > <BR><BR><BR> <STRONG>vs.</STRONG></td>

                                            <td>
                                      
                                            

      
        <button type="button" class="btn btn-default" style="margin-top: 130px"> <?php echo $List2 ?> </button>          
 
                                                        </td>
                                        
                                             
                                            
                                                

                                            <td align="center">
                                                

                                            <?php
                                $aqui= "admin/img/team/".$bb.".png";       
                                  if(file_exists($aqui)){
                                  ?>  
                                  <br>
                                <div class="user-panel" >
            <div class="pull-center image">
                <img src="admin/img/team/<?php echo  $bb?>.png" class="img-circle" alt="User Image" style="border-radius: 50%;
                            height:80px;
                            width: 80px;
                            border: 2px solid;
                            border-color: #46595D;
                          ">
            </div>
            <br>
            <div class="pull-center info">
            <?php echo $bbn ?>
            </div>
        </div>                               
                               
                                  <?php }else{?>
                                    <br>
                                <div class="user-panel" >
            <div class="pull-center image">
                <img src="admin/img/icons/inco.png" class="img-circle" alt="User Image" style="border-radius: 50%;
                            height:80px;
                            width: 80px;
                            border: 2px solid;
                            border-color: #46595D;
                          ">
            </div>
            <br>
            <div class="pull-center info">
            <?php echo $bbn ?>
            </div>
        </div>                             
                                <?php  }?>  
                                        
                                        
                                        </td>

                                            <td ><a href="game_dataa.php?id=<?php echo $info['id_fixture']?>"> <br><br>MAS INFORMACION</a></td>

                                        </tr>

                                        <?php } ?>

                                    </table>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>



                <hr>



                <div class="row">

                    <div class="col">

                        <section class="call-to-action featured featured-primary button-centered" style="">

                            <div class="col-12">

                                <div class="call-to-action-content">

                                    <h4 class="text-uppercase"><strong>TABLA DE POSICIONES</strong></h4>

                                </div>

                            </div>



                            <div class="col-12">



                                <div class="slider_panel">

                                    <ul class="lightSlider">

                                        <?php

                                        if (is_dir($positionImagesPath))

                                        {

                                            $filesList = array_slice(scandir($positionImagesPath), 2); // Without dots



                                            foreach ($filesList as $file)

                                            {

                                                if (is_dir($positionImagesPath . $file))

                                                    echo '';

                                                else if (strpos($file, '.jpg') !== false || strpos($file, '.png') !== false || strpos($file, '.jpeg') !== false)

                                                {

                                                    ?>

                                                    <li data-thumb="<?php echo $positionImagesPath . $file ?>">

                                                        <img src="<?php echo $positionImagesPath . $file ?>"/>

                                                    </li>

                                                    <?php

                                                }

                                            }

                                        }

                                        ?>

                                    </ul>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>



                <hr>



                <div class="row">

                    <div class="col">

                        <section class="call-to-action featured featured-primary button-centered" style="">

                            <div class="col-12">

                                <div class="call-to-action-content">

                                    <h4 class="text-uppercase"><strong>MARCACIONES</strong></h4>

                                    <p class="mb-0">

                                    <table width="100%">

                                        <tr>

                                            <th>FOTO</th>

                                            <th>NOMBRE</th>

                                            <th>NÚMERO</th>

                                            <th>ANOTACIONES</th>

                                        </tr>



                                        <?php

                                        foreach ($scoresList as $player)

                                        { ?>

                                            <tr>

                                                <td>

                                                    <?php
                                                      
                                                  
                                                    $pathPhoto = 'admin/img/team/';
                                                    $pathPhoto1 = 'admin/img/player/';
                                                 
                                                 
                                                    if(empty($player['id_player'])){
                                                        $photoPlayer = $pathPhoto .$player['id_team'] . '.png';
                                                       
                                                    }else{
                                                        $photoPlayer = $pathPhoto1 .$player['id_player'] . '.jpg';
                                                    }
                                                    if (file_exists($photoPlayer))
                                                    
                                                    { ?>
                                                    
                                                        <img src="<?php echo $photoPlayer . '?' . time() ?>" style=" max-height: 150px;border-radius: 50%; height:80px;
                            width: 80px;
                            border: 2px solid;
                            border-color: #46595D;">
                                                    <?php } else{ ?>
                                                        
                                                        <img src="admin/img/icons/inco.png" style=" max-height: 150px;border-radius: 50%; height:80px;
                            width: 80px;
                            border: 2px solid;
                            border-color: #46595D;">
                                                    <?php } ?>

                                                </td>

                                                <td><?php 
                                                if(empty($player['name_player'])){
                                                    echo $player['name_team'];
                                                }else{
                                                    echo $player['name_player'];
                                                }
                                                
                                                ?></td>

                                                <td><?php echo $player['number_player'] ?></td>

                                                <td><?php echo $player['total_goals'] ?></td>

                                            </tr>

                                        <?php }

                                        ?>



                                    </table>

                                    </p>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>



                <hr>



                <div class="row">

                    <div class="col">

                        <section class="call-to-action featured featured-primary button-centered" style="">

                            <div class="col-12">

                                <div class="call-to-action-content">

                                    <h4 class="text-uppercase"><strong>AMONESTACIONES</strong></h4>

                                    <p class="mb-0">

                                    <table width="100%">

                                        <tr>

                                            <th>FOTO</th>

                                            <th>NOMBRE</th>

                                            <th>NÚMERO</th>

                                            <th>AMARILLAS</th>

                                            <th>ROJAS</th>

                                        </tr>



                                        <?php

                                        foreach ($cautionedList as $player)

                                        { ?>

                                            <tr>

                                                <td>

                                                    <?php

                                                    $pathPhoto = 'admin/img/player/';

                                                    $photoPlayer = $pathPhoto . $player['id_player'] . '.jpg';

                                                    if (is_file($photoPlayer))

                                                    {

                                                        ?>

                                                        <img src="<?php echo $photoPlayer . '?' . time() ?>" style="max-height: 150px">

                                                    <?php } else

                                                    { ?>

                                                        <img src="img/no_photo.jpg" style="max-height: 150px">

                                                    <?php } ?>

                                                </td>

                                                <td><?php echo $player['name_player'] ?></td>

                                                <td><?php echo $player['number_player'] ?></td>

                                                <td><?php echo $player['yellow_cards'] ?></td>

                                                <td><?php echo $player['red_cards'] ?></td>

                                            </tr>

                                        <?php }

                                        ?>



                                    </table>

                                    </p>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>



                <hr>



                <div class="row">

                    <div class="col">
                        <section class="call-to-action featured featured-primary button-centered" style="">

                            <div class="col-12">

                                <div class="call-to-action-content">

                                    <h4 class="text-uppercase"><strong>RECEPCIONES</strong></h4>

                                    <p class="mb-0">

                                    <table width="100%">
                                <tr>
                                <th>FOTO</th> 
                                    <th>EQUIPO/CLUB/ATLETA</th>
                                    <th>RESULTADO</th>
                                </tr>
                                <?php
                                foreach($list as $info)
                                {
                                    ?>
                                <tr>
                                <td><?php 
                                
                                $aqui= "admin/img/team/".$info['id_team'].".png";
                                   

                                if(file_exists($aqui)){
                                ?>  
                                <br>
                              <div class="user-panel" >
          <div class="pull-center image">
              <img src="admin/img/team/<?php echo  $info['id_team']?>.png" class="img-circle" alt="User Image" style="border-radius: 50%;
                          height:80px;
                          width: 80px;
                          border: 2px solid;
                          border-color: #46595D;
                        ">
          </div>
          <br>
         
      </div>                               
                             
                                <?php }else{?>
                                  <br>
                              <div class="user-panel" >
          <div class="pull-center image">
              <img src="admin/img/icons/inco.png" class="img-circle" alt="User Image" style="border-radius: 50%;
                          height:80px;
                          width: 80px;
                          border: 2px solid;
                          border-color: #46595D;
                        ">
          </div>
          <br>
         
      </div>                             
                                
      <?php }?>         
                                </td>  

                                    <td><?php echo $info['name_team']?></td>
                                    <td><?php echo $info['resultado']?></td>
                                </tr>
                                <?php 
                                } ?>
                            </table>

                                    </p>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>



                <hr>



                <div class="row">

                    <div class="col">

                        <section class="call-to-action featured featured-primary button-centered" style="">

                            <div class="col-12">

                                <div class="call-to-action-content">

                                    <h4 class="text-uppercase"><strong>NOTICIAS</strong></h4>

                                    <p class="mb-0">

                                    <table width="100%">

                                        <tr>

                                            <th>IMAGEN</th>

                                            <th>CONTENIDO</th>

                                        </tr>



                                        <?php

                                        foreach ($newsList as $news)

                                        {

                                            ?>

                                            <tr>

                                                <td width="40%">

                                                    <?php

                                                    $imagesPath = "admin/img/news/" . $_GET['id'] . '/';

                                                    $file = $imagesPath . $news['id_news'] . '.jpg';

                                                    if (is_file($file))

                                                        echo "<img src='$file' style='max-height: 200px; max-width: 300px'>";

                                                    else

                                                        echo '-- SIN IMAGEN --';

                                                    ?>

                                                </td>

                                                <td><?php echo $news['text_news'] ?></td>

                                            </tr>

                                        <?php }

                                        ?>



                                    </table>

                                    </p>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>



            </div>

        </section>





    </div>



    <?php include 'footer.php' ?>

</div>



<!-- Vendor -->

<script src="vendor/jquery/jquery.min.js"></script>

<script src="vendor/jquery.appear/jquery.appear.min.js"></script>

<script src="vendor/jquery.easing/jquery.easing.min.js"></script>

<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>

<script src="vendor/popper/umd/popper.min.js"></script>

<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<script src="vendor/common/common.min.js"></script>

<script src="vendor/jquery.validation/jquery.validation.min.js"></script>

<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>

<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>

<script src="vendor/isotope/jquery.isotope.min.js"></script>

<script src="vendor/owl.carousel/owl.carousel.min.js"></script>

<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<script src="vendor/vide/vide.min.js"></script>



<!-- Theme Base, Components and Settings -->

<script src="js/theme.js"></script>



<!-- Theme Custom -->

<script src="js/custom.js"></script>



<!-- Theme Initialization Files -->

<script src="js/theme.init.js"></script>



<script src="admin/plugins/lightslider/js/lightslider.js"></script>



<script>



    $(document).ready(function () {

        $(".lightSlider").lightSlider({

            gallery: true,

            item: 1,

            loop: true,

            slideMargin: 0,

            adaptiveHeight: true,

            responsive: [

                {

                    breakpoint: 800,

                    settings: {

                        thumbItem: 3

                    }

                },

                {

                    breakpoint: 480,

                    settings: {

                        thumbItem: 2

                    }

                }

            ]

        });

    });



</script>



</body>

</html>

