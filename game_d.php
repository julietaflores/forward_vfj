<?php
if( !isset( $_GET['id'] ) ){
    header('index.php');
}
date_default_timezone_set('America/La_Paz');
include_once 'admin/connection.php';
include_once 'admin/models/model_fixture.php';
include_once 'admin/models/model_goal_scorer.php';
include_once 'admin/models/model_cautioned_player.php';
include_once ('admin/models/model_tournament.php');

$fixtureModel = new Fixture_Model();
$goalScorerModel = new Goal_Scorer_Model();

$tournamentModel = new Tournament_Model();

$match = $fixtureModel->getAll11f($_GET['id']);


foreach($match as $posicion=>$info)
{  $match['id_tournament']=$info['id_tournament'];
    $match['date_game_fixture']=$info['date_game_fixture'];
    $match['id_fixture']=$info['id_fixture'];
    $match['hour_game_fixture']=$info['hour_game_fixture'];
   $tipot = $info['id_tournament'];
}


$tournament = $tournamentModel->getById( $match['id_tournament']);

foreach($tournament as $posicion=>$tont)
{
   $name = $tont['name_tournament'];
}

?>



<!DOCTYPE html>

<html>

<head>



    <!-- Basic -->

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <title>FORWARD</title>



    <meta name="keywords" content="Forward"/>

    <meta name="description" content="Aplicacion forward">

    <meta name="author" content="IDRA">



    <!-- Favicon -->

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">



    <!-- Mobile Metas -->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">



    <!-- Web Fonts  -->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">



    <!-- Vendor CSS -->

    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css?<?php echo time()?>">

    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" href="vendor/animate/animate.min.css">

    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">

    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">

    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">

    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">



    <!-- Theme CSS -->

    <link rel="stylesheet" href="css/theme.css?<?php echo time()?>">

    <link rel="stylesheet" href="css/theme-elements.css">

    <link rel="stylesheet" href="css/theme-blog.css">

    <link rel="stylesheet" href="css/theme-shop.css">



    <!-- Demo CSS -->





    <!-- Skin CSS -->

    <link rel="stylesheet" href="css/skins/default.css?<?php echo time()?>">



    <!-- Theme Custom CSS -->

    <link rel="stylesheet" href="css/custom.css">



    <!-- Head Libs -->

    <script src="vendor/modernizr/modernizr.min.js"></script>



</head>

<body>



<div class="body">

    <?php include 'sidebar.php' ?>



    <div role="main" class="main">



        <section class="page-header">

            <div class="container">

                <div class="row">

                    <div class="col">

                        <h1>Datos del encuentro/partido</h1>

                    </div>

                </div>

            </div>

        </section>



        <div class="container">



            <div class="featured-boxes">

                <div class="row">

                    <div class="col-lg-6 col-sm-6">

                        <div class="featured-box featured-box-primary featured-box-effect-1 mt-0 mt-lg-5">

                            <div class="box-content">

                                <i class="icon-featured fa fa-soccer-ball-o"></i>

                                <p>Torneo <?php echo $name ?></p>

                                <p><?php echo $match['hour_game_fixture'] ?></p>



                                <table width="100%">

                                    <tr>

                                        <td colspan="3">

                                        <?php  
                
                echo '<h3>NO HAY RESULTADOS DISPONIBLE EN ESTE MOMENTO!</h3>';
                ?>

                                        </td>

                                    </tr>



                                   
                                    <tr>



                                    </tr>

                               

                                </table>



                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



    <?php include 'footer.php' ?>

</div>



<!-- Vendor -->

<script src="vendor/jquery/jquery.min.js"></script>

<script src="vendor/jquery.appear/jquery.appear.min.js"></script>

<script src="vendor/jquery.easing/jquery.easing.min.js"></script>

<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>

<script src="vendor/popper/umd/popper.min.js"></script>

<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<script src="vendor/common/common.min.js"></script>

<script src="vendor/jquery.validation/jquery.validation.min.js"></script>

<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>

<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>

<script src="vendor/isotope/jquery.isotope.min.js"></script>

<script src="vendor/owl.carousel/owl.carousel.min.js"></script>

<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<script src="vendor/vide/vide.min.js"></script>



<!-- Theme Base, Components and Settings -->

<script src="js/theme.js"></script>



<!-- Theme Custom -->

<script src="js/custom.js"></script>



<!-- Theme Initialization Files -->

<script src="js/theme.init.js"></script>






</body>

</html>


