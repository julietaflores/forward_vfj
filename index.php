<?php
include_once 'admin/connection.php';
include_once 'admin/models/model_event.php';

$eventModel = new Event_Model();
$eventList = $eventModel->getAll();



?>

<!DOCTYPE html>
<html>
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>FORWARD</title>

    <meta name="keywords" content="Forward"/>
    <meta name="description" content="Aplicacion forward">
    <meta name="author" content="IDRA">

    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css?<?php echo time()?>">
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendor/animate/animate.min.css">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="css/theme.css?<?php echo time()?>">
    <link rel="stylesheet" href="css/theme-elements.css">
    <link rel="stylesheet" href="css/theme-blog.css">
    <link rel="stylesheet" href="css/theme-shop.css">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="vendor/rs-plugin/css/settings.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/layers.css">
    <link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css">
    <link rel="stylesheet" href="vendor/circle-flip-slideshow/css/component.css">

    <!-- Demo CSS -->


    <!-- Skin CSS -->
    <link rel="stylesheet" href="css/skins/default.css?<?php echo time()?>">

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Head Libs -->
    <script src="vendor/modernizr/modernizr.min.js"></script>

</head>
<body>
<div class="body">
    <?php include 'sidebar.php' ?>

    <div role="main" class="main">

        <section class="call-to-action call-to-action-tertiary call-to-action-big mb-0" style="background-color: #CAD41E">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-lg-12">
                        <div class="call-to-action-content">
                            <h2 class="text-color-light mb-0">Disfruta informado con <strong>Forward Mundial</strong></h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-default-scale-lighten m-0">
            <div class="container-fluid sample-item-container">

                <div class="row text-center">
                    <div class="col-lg-12 pb-1">
                        <h4><b>EVENTOS</b></h4>
                    </div>
                </div>

                <div class="row sample-item-list sample-item-list-loaded" data-plugin-masonry data-plugin-options="{'itemSelector': '.isotope-item'}">
                    <?php
                    foreach ($eventList as $event){
                    ?>
                    <div class="col-md-6 isotope-item" align="center">
                        <div class="sample-item sample-item-home pl-3 pr-3">
                            <a href="tournament.php?id=<?php echo $event['id_event'] ?>">
                                <span class="sample-item-image-wrapper">

                                    <span class="sample-item-image">
                                          
                                            <?php
                                              $er="admin/img/event/".$event['id_event'].".jpg";
                                              $err= $er.time();
                                             if(file_exists($er)){?>
  <img src="admin/img/event/<?php echo $event['id_event'] ?>.jpg?<?php echo time() ?>" height="200px">
                                            <?php }else{?>
                                                <img src="admin/img/logos/logo.jpg" height="200px">
                                            <?php }?>

                                            
                                    </span>
                                    <i class="fa fa-spinner fa-spin fa-fw"></i>
								</span>
                                <span class="sample-item-description">
                                    <h5 style="color: green"><?php echo $event['name_event'] ?></h5>
                                </span>
                            </a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </section>
    </div>

    <?php include 'footer.php'?>
</div>

<!-- Vendor -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
<script src="vendor/popper/umd/popper.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/common/common.min.js"></script>
<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="vendor/isotope/jquery.isotope.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="vendor/vide/vide.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="js/theme.js"></script>

<!-- Current Page Vendor and Views -->
<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="vendor/circle-flip-slideshow/js/jquery.flipshow.min.js"></script>
<script src="js/views/view.home.js"></script>

<!-- Theme Custom -->
<script src="js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="js/theme.init.js"></script>

<!-- Examples -->
<script src="js/examples/examples.demos.js"></script>

</body>
</html>