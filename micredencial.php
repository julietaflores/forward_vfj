<?php
require "admin/views/phpqrcode/qrlib.php";    
include_once('admin/configs.php');
// include_once('../session_manager.php');
include('admin/connection.php');
include('admin/models/model_credential.php');
include('admin/models/model_player.php');
include('admin/models/model_team.php');
$teamModel = new Team_Model();
$playerModel = new Player_Model();
$credentialModel = new Credential_Model();




if (isset($_GET['id'])){
    $player = $playerModel->getById($_GET['id']);



    $credential = $credentialModel->getByPlayerId($_GET['id']);
    if($credential){
        foreach ($credential AS $id => $info){           
           $credential['id_credential']= $info['id_credential'];
           $credential['id_player']= $info['id_player'];
           $credential['date_begin_credential']=$info['date_begin_credential'];
           $credential['date_end_credential']=$info['date_end_credential']; 
           $credential['field_1_credential']=$info['field_1_credential'];
           $credential['field_2_credential']=$info['field_2_credential'];
           $credential['field_3_credential']=$info['field_3_credential'];
      
           $dd1=$info['field_1_credential'];
           $dd2=$info['field_2_credential'];
           $dd3=$info['field_3_credential'];
          }


          if($player){
            foreach ($player AS $id => $info){ 
                $player['id_player']=$info['id_player'];
                $player['name_player'] = $info['name_player'];
                $player['lastname_player'] = $info['lastname_player'];
                $player['lastname2_player'] = $info['lastname2_player'];
                $dd1=$info[$dd1];
                $dd2=$info[$dd2];
                $dd3=$info[$dd3];
              }
         }
    
     }

    
       if($credential==0){
         $coco=0;
       }else{
        $coco=1;
       }

      



     

    
$dir = 'temp/';
	
if (!file_exists($dir))
    mkdir($dir);

$filename = $dir.'test.png';

$tamaño = 4; //Tamaño de Pixel
$level = 'L'; //Precisión Baja
$framSize = 1; //Tamaño en blanco

}else{
    header('Location: list_player.php');
}



if (!$credential)
    $credential = $credentialModel->getEmpty();

$message = '';
if (count($_POST) > 0)
{
    include('../util/validators.php');
    $validator = new Validators();
    $message .= $validator->isValidDate($_POST['date_begin_player_credential'], 'Fecha de habilitación');
    $message .= $validator->isValidDate($_POST['date_end_player_credential'], 'Fecha de vencimiento');
    $message .= $validator->isNotEmpty($_POST['field_1_credential'], 'Dato 1');
    $message .= $validator->isNotEmpty($_POST['field_2_credential'], 'Dato 2');
    $message .= $validator->isNotEmpty($_POST['field_3_credential'], 'Dato 3');

    if (empty($message))
    {
        if ( empty($credential['id_credential']) )
        {
            $idCredential = $credentialModel->save(
                $_GET['id'],
                $_POST['field_1_credential'],
                $_POST['field_2_credential'],
                $_POST['field_3_credential'],
                $_POST['date_begin_player_credential'],
                $_POST['date_end_player_credential']
            );
        } else
        {
            if ($credentialModel->update(
                $credential['id_credential'],
                $_POST['field_1_credential'],
                $_POST['field_2_credential'],
                $_POST['field_3_credential'],
                $_POST['date_begin_player_credential'],
                $_POST['date_end_player_credential']

            ))
                $idCredential = $credential['id_credential'];
        }

        if ($idCredential)
            header('Location: list_player.php');
        else
            $message = 'Ocurrio un error al registrar el credencial del atleta.';
    } else
    {
        $credential['date_begin_player_credential'] = $_POST['date_begin_player_credential'];
        $credential['date_end_player_credential'] = $_POST['date_end_player_credential'];
        $credential['field_1_credential'] = $_POST['field_1_credential'];
        $credential['field_2_credential'] = $_POST['field_2_credential'];
        $credential['field_3_credential'] = $_POST['field_3_credential'];
    }
}

$options = array();
$options['Fecha de Nacimiento'] = 'born_date_player';
$options['Edad'] = 'code_age';
$options['Función'] = 'function_player';
$options['Carnet de Identidad'] = 'ci_player';
$options['País'] = 'country_player';
$options['Ciudad'] = 'city_player';
$options['Club'] = 'club_player';
$options['Deporte'] = 'sport_player';
$options['Categoría'] = 'category_player';
$options['Género'] = 'gender_player';
$options['División'] = 'division_player';
$options['Torneo'] = 'tournament_player';
$options['Nivel'] = 'level_player';
$options['Tipo de sangre'] = 'blood_type_player';
$options['Altura'] = 'height_player';
$options['Peso'] = 'weight_player';
$options['IMC'] = 'code_imc';
$options['Email'] = 'email_player';
$options['Teléfono'] = 'cellphone_player';
$options['Nombre de referencia'] = 'name_reference_player';
$options['Teléfono de referencia'] = 'phone_reference_player';
$options['Curriculum Deportivo'] = 'curriculum_vitae_player';


?>
<!DOCTYPE html>
<html>

<style type="text/css">	


.containerr {
	height: 405px;
	margin: auto;
	width: 255.6px;
	-webkit-perspective: 750;
}

.carta {
	height: 100%;
	width: 100%;
	transition: all 1s;
	transform-style: preserve-3d;
	box-shadow: 0px 0px 5px black;
	position: absolute;
}
.carta1 {
	height: 100%;
	width: 100%;
	transition: all 1s;
	transform-style: preserve-3d;
	box-shadow: 0px 0px 5px black;
	position: absolute;
	border: solid 1px gray;
	border-radius: 3px;
}

.carta:hover{
  transform: rotateY(180deg);
}

.lado{
	position: absolute;
	height: 100%;
	width: 100%;
   background: #F0F0F0;
}
.lado1{
	position: absolute;
	height: 100%;
	width: 100%;
   background: #F0F0F0;
}

.frente{ 
    background-color: white;
	width: 100%;
 }

 .frente1{ 
    background-color: white;
    width: 100%;
    height: 20%;
    text-align: center;
    border-bottom: solid 9px;
 }



 .frente2{ 
    margin-top: 70px; 
    background-color: black;
    height: 5%;
	width: 100%;
 }

 .frente3{ 
    margin-top: 85px; 
    /*background-color: yellow;*/
    height: 75%;
	width: 100%;
 }


.ull {
  
    margin-top: 5px;

    margin-right: 5px;
}


.ull1 {
    list-style: none;

    margin-top: 15px;
    margin-left: -19px;
    margin-right: 5px;
}



.liii1 {
	text-decoration: none;
	padding: 10px;
  z-index:80;
}

.lii {
	text-decoration: none;
	padding: 10px;
    z-index:80;
}


.liii {
    list-style: none;
    text-align: center;
}
}






.li2 {
    list-style: none;

    margin: 5px 5px 5px 5px;
    text-align: center;
}


.atras{
	height: 100%;
	width: 100%;
  transform: rotateY(180deg);
}




</style>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo WEB_TITLE; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/bootstrap/css/bootstrap.min.css">
    <!-- JqueryUI -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/ionicons-2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/dist/css/skins/skin-red.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo LOCALHOST; ?>/plugins/iCheck/flat/red.css">

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo LOCALHOST; ?>/plugins/jquery-ui-1.12.0/jquery-ui.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo LOCALHOST; ?>/bootstrap/js/bootstrap.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo LOCALHOST; ?>/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo LOCALHOST; ?>/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo LOCALHOST; ?>/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo LOCALHOST; ?>/dist/js/app.min.js"></script>
</head>

<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">



    <div class="content-wrapper">


        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-6">
                    <input type="button" onClick="printPage();" class="btn btn-primary" value="Imprimir">
                    
                    <script>
                    
                    function printPage() {
                        print();
                    }
                    </script>
                </div>




                <div class="col-md-1">
                </div>


        <?php if($coco==0){
           // echo "Sin credencial todavia";
            ?>

        
        
        <?php }else{?>


            <div class="col-md-4" >
        <div class="containerr">
       

        <div class="carta1">
    
    <div class="lado frente">




            <div class="lado frente1">
                <img  src="<?php echo LOCALHOST;?>/img/logos/logo_credencial.jpeg" style="height:50px;margin-top: 5px;left: 1px !important;   position: absolute;" alt="">


                     <h4  id="m1" name="m1" style="font-size: 15px;
    font-weight: bold;
    margin-left: 70px;
    padding-top: 10px;display: inline-block;margin-bottom: 0px !important"><?php echo $player['name_player']; ?>
                     <br>
                    <?php echo $player['lastname_player']; ?> <?php echo $player['lastname2_player']; ?></h4>
                    
            </div>
               

     
                 <div class="lado frente3">
                     <div class="row">
                     <div class="col-md-12" style="text-align: center;">

                        <?php
                                    $photoPlayer = 'admin/img/player/' . $player['id_player'] . '.jpg';
                                    if (is_file($photoPlayer)){ 
                                        ?>
                                        <img src="<?php echo $photoPlayer?>" style="height: 130px;
    width: 120px;
    margin-top: 5px;
    border: 2px solid;
    border-color: black;
    border-radius: 10%;" alt="">
                                    <?php }else{ ?>

                                        <img src="<?php echo LOCALHOST;?>/img/icons/user-icon.png" style="height: 130px;
    width: 120px;
    margin-top: 5px;
    border: 2px solid;
    border-color: black;
    border-radius: 10%;" alt="">
                                        <?php } ?>

                   
                     </div>
                     </div>
               


                     <div class="row" >

                     <div class="col-md-4">
                    
                     
                     
                  </div>
                     </div>
               


                    <div class="row">
                        <div class="col-md-12" style="text-align: center;">
                         <p>
                  
                          <?php  echo substr( $dd1,0,42).' ';?><bR>
    			          <?php  echo substr( $dd2,0,15).' ';?><bR>
    			          <?php  echo substr( $dd3,0,15).'';?>
                            </p>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12" style="text-align: center;">
                                     
                         <?php 
                        $UN_SALTO="\r\n";
                        $cdd= $dd1.$UN_SALTO.$dd2.$UN_SALTO.$dd3;
                        $idplayer=$player['id_player'];
                        $contenido = $player['name_player']." ".$player['lastname_player']." ".$player['lastname2_player'];
                        
                        QRcode::png($idplayer." id usuario: ".$idplayer.$UN_SALTO.$UN_SALTO."Nombre Completo: ".$contenido.$UN_SALTO.$UN_SALTO."Detalles: ".$UN_SALTO.$cdd, $filename, $level, $tamaño, $framSize); 
                        
                        echo '<img src="'.$dir.basename($filename).'" style="width:90px;" />';   
                        ?>                     
                             
    			
                        </div>

                    </div>
    
              <footer class="row">
                <li class="liii" style="font-weight: 600; ">Santa Cruz - Bolivia</li>        
              </footer>
    
          </div>



    </div>

    

</div>

   

         </div>
            </div>


            <?php }?>



            </div>
        </section>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
</div>

</body>
</html>

