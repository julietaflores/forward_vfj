<?php
include_once 'admin/connection.php';
include_once 'admin/models/model_tournament.php';
include_once 'admin/models/model_fixture.php';

$tournamentModel = new Tournament_Model();
$tournamentListDesc = $tournamentModel->getAllDesc();

$fixtureModel = new Fixture_Model();

if( isset($_GET['id']) )
    $tournamentFixture = $fixtureModel->getAll($_GET['id']);
else
    $tournamentFixture = $fixtureModel->getAll($tournamentListDesc[0]['id_tournament']);
?>

<!DOCTYPE html>
<html>
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>FORWARD</title>

    <meta name="keywords" content="Forward"/>
    <meta name="description" content="Aplicacion forward">
    <meta name="author" content="IDRA">

    <!-- Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css?<?php echo time()?>">
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendor/animate/animate.min.css">
    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="css/theme.css?<?php echo time()?>">
    <link rel="stylesheet" href="css/theme-elements.css">
    <link rel="stylesheet" href="css/theme-blog.css">
    <link rel="stylesheet" href="css/theme-shop.css">

    <!-- Demo CSS -->


    <!-- Skin CSS -->
    <link rel="stylesheet" href="css/skins/default.css?<?php echo time()?>">

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Head Libs -->
    <script src="vendor/modernizr/modernizr.min.js"></script>

    <script src="vendor/jquery/jquery.min.js"></script>

    <script>
        $(document).ready(function(){
            $('#choose').change(function(){
                $('#choose_tournament').submit();
            });
        });
    </script>

</head>
<body>

<div class="body">
    <?php include 'sidebar.php' ?>

    <div role="main" class="main">

        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h1>Partidos programados</h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">

            <div class="row">
                <div class="col">
                    <h4 class="mb-0">Seleccione un torneo:</h4>
                    <form id="choose_tournament" method="get">
                        <select id="choose" name="id">
                            <?php foreach ($tournamentListDesc as $tournament) { ?>
                                <option value="<?php echo $tournament['id_tournament'] ?>" <?php if(isset($_GET['id'])) if($_GET['id'] == $tournament['id_tournament']) echo 'selected'?>><?php echo $tournament['name_tournament'] ?></option>
                            <?php } ?>
                        </select>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <hr class="tall">

                    <?php foreach($tournamentFixture as $match){ ?>
                    <section class="call-to-action featured featured-primary button-centered" style="">
                        <div class="col-12" >
                            <div class="call-to-action-content">
                                <h4 class="text-uppercase"><strong><?php echo $match['t1Name']?></strong> vs <strong><?php echo $match['t2Name']?></strong></h4>
                                <p class="mb-0">
                                    <?php echo DateTime::createFromFormat('Y-m-d', $match['date_game_fixture'])->format('d/m/Y')?>
                                    <br>
                                    <?php echo DateTime::createFromFormat('H:i:s', $match['hour_game_fixture'])->format('H:i')?>
                                    <br>
                                    CANCHA: <?php echo $match['stadium_number_fixture']?>
                                </p>
                            </div>
                        </div>
                    </section>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>

    <?php include 'footer.php' ?>
</div>

<!-- Vendor -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/jquery.appear/jquery.appear.min.js"></script>
<script src="vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>
<script src="vendor/popper/umd/popper.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/common/common.min.js"></script>
<script src="vendor/jquery.validation/jquery.validation.min.js"></script>
<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>
<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
<script src="vendor/isotope/jquery.isotope.min.js"></script>
<script src="vendor/owl.carousel/owl.carousel.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="vendor/vide/vide.min.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="js/theme.js"></script>

<!-- Theme Custom -->
<script src="js/custom.js"></script>

<!-- Theme Initialization Files -->
<script src="js/theme.init.js"></script>

<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-12345678-1', 'auto');
    ga('send', 'pageview');
</script>
 -->

</body>
</html>
