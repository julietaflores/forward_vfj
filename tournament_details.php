<?php

if (!isset($_GET['id'])){
    header('Location: index.php');
}

 
date_default_timezone_set('America/La_Paz');
include_once 'admin/connection.php';
include_once 'admin/models/model_tournament.php';


$tournamentModel = new Tournament_Model();
$tournament = $tournamentModel->getById($_GET['id']);



if($tournament){
    foreach ($tournament AS $id => $info){
        $tournament['id_tournament']=$info['id_tournament'];
        $tournament['id_event'] = $info['id_event'];
        $tournament['id_sport'] = $info['id_sport'];
        $tournament['id_category'] = $info['id_category'];
        $tournament['date_tournament']=$info['date_tournament'];
        $tournament['name_tournament'] = $info['name_tournament'];
        $tournament['gender_tournament']=$info['gender_tournament'];
        $tournament['type_tournament'] = $info['type_tournament'];
        $tournament['number_series_tournament'] = $info['number_series_tournament'];
        $tournament['number_teams_by_serie_tournament'] = $info['number_teams_by_serie_tournament'];
        $tournament['schema_keys_tournament'] = $info['schema_keys_tournament'];
        $tournament['status_tournament'] = $info['status_tournament'];
        $tournament['tiempos'] = $info['tiempos'];
        $tournament['nsembrado'] = $info['nsembrado'];
        $tournament['tipot'] = $info['tipot'];
        $tournament['name_category'] = $info['name_category'];
        $tournament['name_sport'] = $info['name_sport'];
        $tournament['name_event'] = $info['name_event'];
       
    }
}



if (!$tournament){
    header('Location: index.php');
}


include_once 'admin/models/model_tournament_teams.php';
include_once 'admin/models/model_fixture.php';
include_once 'admin/models/model_goal_scorer.php';
include_once('admin/models/model_cautioned_player.php');
include_once('admin/models/model_player.php');
include_once('admin/models/model_news.php');



//---------------------------------------------------

$tournamentTeamsModel = new Tournament_Teams_Model();

$tournamentTeamsList = $tournamentTeamsModel->getByTournament($_GET['id']);



//---------------------------------------------------

$fixtureImagesPath = "admin/img/fixture/" . $_GET['id'] . '/';



//---------------------------------------------------

$fixtureModel = new Fixture_Model();

$matchesUntilToday = $fixtureModel->getAllByTournamentByDate($_GET['id'], date('Y-m-d'));



$goalScorerModel = new Goal_Scorer_Model();



//---------------------------------------------------

$positionImagesPath = "admin/img/position/" . $_GET['id'] . '/';



//---------------------------------------------------

$goalScorerModel = new Goal_Scorer_Model();

$scoresList = $goalScorerModel->getByTournamentTotalGoals($_GET['id']);



//---------------------------------------------------

$cautionedPlayerModel = new Cautioned_Player_Model();

$cautionedList = $cautionedPlayerModel->getByTournamentTotalCations($_GET['id']);



//---------------------------------------------------

$listGoals = array();

$listIdGoalkeepers = array();



$fixtureList = $fixtureModel->getAllByTournament($_GET['id']);

foreach ($fixtureList as $fixture)

{

    if (!empty($fixture['id_goalkeeper_1']))

    {

        $listIdGoalkeepers[] = $fixture['id_goalkeeper_1'];

        $ss1 = $goalScorerModel->getGoalByTournamentByGoalkeeper($_GET['id'], $fixture['id_goalkeeper_1']);


        if($ss1){
            foreach ($ss1 AS $id => $info){
                $gol1=$info['contador'];
            }
        }
        
        $listGoals[]=$gol1;

    }



    if (!empty($fixture['id_goalkeeper_2']))

    {

        $listIdGoalkeepers[] = $fixture['id_goalkeeper_2'];

        $ss2 = $goalScorerModel->getGoalByTournamentByGoalkeeper($_GET['id'], $fixture['id_goalkeeper_2']);

        if($ss2){
            foreach ($ss2 AS $id => $info){
                $gol2=$info['contador'];
            }
        }
        
        $listGoals[]=$gol2;

    }

}

array_multisort($listGoals, SORT_ASC, $listIdGoalkeepers);

$playerModel = new Player_Model();



//---------------------------------------------------

$newsModel = new News_Model();

$newsList = $newsModel->getAllByTournament($tournament['id_tournament']);

?>



<!DOCTYPE html>

<html>

<head>



    <!-- Basic -->

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">



    <title>FORWARD</title>



    <meta name="keywords" content="Forward"/>

    <meta name="description" content="Aplicacion forward">

    <meta name="author" content="IDRA">



    <!-- Favicon -->

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon"/>

    <link rel="apple-touch-icon" href="img/apple-touch-icon.png">



    <!-- Mobile Metas -->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">



    <!-- Web Fonts  -->

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">



    <!-- Vendor CSS -->

    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css?<?php echo time() ?>">

    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" href="vendor/animate/animate.min.css">

    <link rel="stylesheet" href="vendor/simple-line-icons/css/simple-line-icons.min.css">

    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.min.css">

    <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.min.css">

    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.min.css">



    <!-- Theme CSS -->

    <link rel="stylesheet" href="css/theme.css?<?php echo time() ?>">

    <link rel="stylesheet" href="css/theme-elements.css">

    <link rel="stylesheet" href="css/theme-blog.css">

    <link rel="stylesheet" href="css/theme-shop.css">



    <link rel="stylesheet" href="admin/plugins/lightslider/css/lightslider.css">



    <style>

        table td {

            border-top: 1px #CAD41E dashed;

        }



        .slider_panel {

            width: 100%;

            background-color: #000000;

            padding: 10px;

        }



        .slider_panel ul {

            list-style: none outside none;

            padding-left: 0;

            margin-bottom: 0;

        }



        .slider_panel ul li {

            display: block;

            float: left;

            margin-right: 6px;

            cursor: pointer;

        }



        .slider_panel ul li img {

            display: block;

            height: auto;

            max-width: 100%;

        }



        .lSSlideOuter .lSPager.lSGallery img {

            max-height: 80px;

        }

    </style>





    <!-- Demo CSS -->





    <!-- Skin CSS -->

    <link rel="stylesheet" href="css/skins/default.css?<?php echo time() ?>">



    <!-- Theme Custom CSS -->

    <link rel="stylesheet" href="css/custom.css">



    <!-- Head Libs -->

    <script src="vendor/modernizr/modernizr.min.js"></script>



    <script src="vendor/jquery/jquery.min.js"></script>





</head>

<body>



<div class="body">

    <?php include 'sidebar.php' ?>



    <div role="main" class="main">



        <section class="page-header">

            <div class="container">

                <div class="row">

                    <div class="col">

                        <h1>Torneos</h1>

                    </div>

                </div>

            </div>

        </section>



        <section class="section section-default-scale-lighten m-0">

            <div class="container-fluid sample-item-container">



                <div class="row text-center">

                    <div class="col-lg-12 pb-1">

                        <h4><b>TORNEO: <span style="font-style:italic"><?php echo $tournament['name_tournament'] ?></span></b></h4>

                    </div>

                </div>



                <div class="row">

                    <div class="col">

                        <section class="call-to-action featured featured-primary button-centered" style="">

                            <div class="col-12">

                                <div class="call-to-action-content">

                                    <h4 class="text-uppercase"><strong>EQUIPOS</strong></h4>

                                    <p class="mb-0">

                                        <?php

                                        foreach ($tournamentTeamsList as $team)

                                        {

                                            echo '<p><a href="team.php?id=' . $team['id_team'] . '">' . $team['name_team'] . '</a></p>';

                                        }

                                        ?>

                                    </p>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>



                <hr>



                <div class="row">

                    <div class="col">

                        <section class="call-to-action featured featured-primary button-centered" style="">

                            <div class="col-12">

                                <div class="call-to-action-content">

                                    <h4 class="text-uppercase"><strong>SORTEO</strong></h4>

                                    <p class="mb-0">

                                    <div class="slider_panel">

                                        <ul class="lightSlider">

                                            <?php

                                            if (is_dir($fixtureImagesPath))

                                            {

                                                $filesList = array_slice(scandir($fixtureImagesPath), 2); // Without dots



                                                foreach ($filesList as $file)

                                                {

                                                    if (is_dir($fixtureImagesPath . $file))

                                                        echo '';

                                                    else if (strpos($file, '.jpg') !== false || strpos($file, '.png') !== false || strpos($file, '.jpeg') !== false)

                                                    {

                                                        ?>

                                                        <li data-thumb="<?php echo $fixtureImagesPath . $file ?>">

                                                            <img style="width:100%;" src="<?php echo $fixtureImagesPath . $file ?>"/>

                                                        </li>

                                                        <?php

                                                    }

                                                }

                                            }

                                            ?>

                                        </ul>

                                    </div>

                                    </p>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>



                <hr>



                <div class="row">

                    <div class="col">

                        <section class="call-to-action featured featured-primary button-centered" style="">

                            <div class="col-12">

                                <div class="call-to-action-content">

                                    <h4 class="text-uppercase"><strong>RESULTADOS</strong></h4>


<br>
                                    <table width="100%" class="table table-striped table-bordered">

                                        <tr>

                                            <!--<th><strong>HORA</strong></th>-->

                                            <th style="text-align: center"><strong>EQUIPO</strong></th>

                                            <th></th>

                                            <th></th>

                                            <th></th>

                                            <th style="text-align: center"><strong>EQUIPO</strong></th>
                                            <th style="text-align: center"><strong>INFORMACION</strong></th>
                                        </tr>

                                        <?php

                        // var_dump($matchesUntilToday);
                                        foreach ($matchesUntilToday as $match)

                                        {

                                            $goals1 = $goals2 = 0;



                                            $goalsGame = $goalScorerModel->getAllByFixture($match['id_fixture']);



                                            foreach ($goalsGame as $goal)
                                            {
                                                if($match['id_goalkeeper_1'] == $goal['id_goalkeeper'])

                                                    $goals2++; // If goalkeeper 2 was scored, then team 1 has plus goal

                                                else if($match['id_goalkeeper_2'] == $goal['id_goalkeeper'])

                                                    $goals1++; // If goalkeeper 1 was scored, then team 2 has plus goal

                                            }

                                       

                                        ?>

                                        <tr>

                                            <!--<td><?php echo DateTime::createFromFormat('H:i:s', $match['hour_game_fixture'])->format('H:i') ?></td>-->

                                            <td align="center">
                                   
                              <?php  if($match['t1Name'] ==""){?>              
                                <br>
                                <div class="user-panel" >
            <div class="pull-center image">
                <img src="admin/img/icons/inco.png" class="img-circle" alt="User Image" style="border-radius: 50%;
                            height:80px;
                            width: 80px;
                            border: 2px solid;
                            border-color: #46595D;
                          ">
            </div>
          
        </div>
        <br>       

                              <?php  }else{
                                  
                                  $aqui= "admin/img/team/".$match['id_team_1'].".png";
                                   

                                  if(file_exists($aqui)){
                                  ?>  
                                  <br>
                                <div class="user-panel" >
            <div class="pull-center image">
                <img src="admin/img/team/<?php echo  $match['id_team_1']?>.png" class="img-circle" alt="User Image" style="border-radius: 50%;
                            height:80px;
                            width: 80px;
                            border: 2px solid;
                            border-color: #46595D;
                          ">
            </div>
            <br>
            <div class="pull-center info">
            <?php echo $match['t1Name']  ?>
            </div>
        </div>                               
                               
                                  <?php }else{?>
                                    <br>
                                <div class="user-panel" >
            <div class="pull-center image">
                <img src="admin/img/icons/inco.png" class="img-circle" alt="User Image" style="border-radius: 50%;
                            height:80px;
                            width: 80px;
                            border: 2px solid;
                            border-color: #46595D;
                          ">
            </div>
            <br>
            <div class="pull-center info">
            <?php echo $match['t1Name']  ?>
            </div>
        </div>                             
                                <?php  }}?>  

                                            
   
                                            
                                              
                                        
                                                
                                        
                                            </td>

                                            <td>                                         
                                        <button type="button" class="btn btn-default" style="margin-top: 80px; font-size:2.5em;"> <?php echo  $goals1 ?> </button>          
                                    
                                        </td>

                                            <td align="center" > <BR><BR><BR> <STRONG>vs.</STRONG> 
                                                                <br> <?php echo DateTime::createFromFormat('H:i:s', $match['hour_game_fixture'])->format('H:i') ?> 
                                                                <br><br><?php echo $match['date_game_fixture'] ?>  
                                                                </td>

                                            <td>
                                                
                                                <button type="button" class="btn btn-default" style="margin-top: 80px; font-size:2.5em;"><?php echo  $goals2 ?> </button>          
                                                        </td>
                                        

                                            <td align="center">
                                                
                                            <?php  if($match['t2Name'] ==""){?>              
                                                <br>
                                <div class="user-panel" >
            <div class="pull-center image">
                <img src="admin/img/icons/inco.png" class="img-circle" alt="User Image" style="border-radius: 50%;
                            height:80px;
                            width: 80px;
                            border: 2px solid;
                            border-color: #46595D;
                          ">
            </div>
          
        </div>  
        <br>     

                              <?php  }else{ ?> 
       
                                   <?php 
                                     

                                  if(file_exists('admin/img/team/'.$match['id_team_2'].'.png')){
                                  ?>  
                                  <br>
                                <div class="user-panel" >
            <div class="pull-center image">
                <img src="admin/img/team/<?php echo  $match['id_team_2']?>.png" class="img-circle" alt="User Image" style="border-radius: 50%;
                            height:80px;
                            width: 80px;
                            border: 2px solid;
                            border-color: #46595D;
                          ">
            </div>
            <br>
            <div class="pull-center info">
            <?php echo $match['t2Name']  ?>
            </div>
        </div> 
        <br>                              
                               
                                  <?php }else{ ?>
                                    <br>
                                <div class="user-panel" >
            <div class="pull-center image">
                <img src="admin/img/icons/inco.png" class="img-circle" alt="User Image" style="border-radius: 50%;
                            height:80px;
                            width: 80px;
                            border: 2px solid;
                            border-color: #46595D;
                          ">
            </div>
            <br>
            <div class="pull-center info">
            <?php echo $match['t2Name']  ?>
            </div>
        </div>
        <br> 

                                <?php }
                                         }?>  
                 
                                        
                                        
                                        </td>

                                            <td ><a href="game_data.php?id=<?php echo $match['id_fixture'] ?>"> <br><br>MAS INFORMACION</a></td>

                                        </tr>

                                        <?php } ?>

                                    </table>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>



                <hr>



                <div class="row">

                    <div class="col">

                        <section class="call-to-action featured featured-primary button-centered" style="">

                            <div class="col-12">

                                <div class="call-to-action-content">

                                    <h4 class="text-uppercase"><strong>TABLA DE POSICIONES</strong></h4>

                                </div>

                            </div>



                            <div class="col-12">



                                <div class="slider_panel">

                                    <ul class="lightSlider">

                                        <?php

                                        if (is_dir($positionImagesPath))

                                        {

                                            $filesList = array_slice(scandir($positionImagesPath), 2); // Without dots



                                            foreach ($filesList as $file)

                                            {

                                                if (is_dir($positionImagesPath . $file))

                                                    echo '';

                                                else if (strpos($file, '.jpg') !== false || strpos($file, '.png') !== false || strpos($file, '.jpeg') !== false)

                                                {

                                                    ?>

                                                    <li data-thumb="<?php echo $positionImagesPath . $file ?>">

                                                        <img style="width:100%;" src="<?php echo $positionImagesPath . $file ?>"/>

                                                    </li>

                                                    <?php

                                                }

                                            }

                                        }

                                        ?>

                                    </ul>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>



                <hr>



                <div class="row">

                    <div class="col">

                        <section class="call-to-action featured featured-primary button-centered" style="">

                            <div class="col-12">

                                <div class="call-to-action-content">

                                    <h4 class="text-uppercase"><strong>MARCACIONES</strong></h4>

                                    <p class="mb-0">

                                    <table width="100%">

                                        <tr>

                                            <th>NÚMERO</th>


                                            <th>FOTO</th>

                                            <th>NOMBRE</th>

                                            <th>APELLIDO</th>

                                            <th>CLUB</th>
                                            
                                            <th>ANOTACIONES</th>

                                        </tr>



                                        <?php
                                        
                                        foreach ($scoresList as $player)

                                        { ?>

                                            <tr>
                                                <td><?php echo $player['number_player'] ?></td>

                                                <td>

                                                    <?php

                                                    $pathPhoto = 'admin/img/player/';

                                                    $photoPlayer = $pathPhoto . $player['id_player'] . '.jpg';

                                                    if (is_file($photoPlayer))

                                                    {

                                                        ?>

                                                        <img src="<?php echo $photoPlayer . '?' . time() ?>" style="max-height: 150px">

                                                    <?php } else

                                                    { ?>

                                                        <img src="img/no_photo.jpg" style="max-height: 150px">

                                                    <?php } ?>

                                                </td>

                                                <td><?php echo $player['name_player'] ?></td>
                                                <td><?php echo $player['lastname_player'] ?></td>
                                                <td><?php echo $player['club_player'] ?></td>


                                                <td><?php echo $player['total_goals'] ?></td>

                                            </tr>

                                        <?php }

                                        ?>



                                    </table>

                                    </p>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>



                <hr>



                <div class="row">

                    <div class="col">

                        <section class="call-to-action featured featured-primary button-centered" style="">

                            <div class="col-12">

                                <div class="call-to-action-content">

                                    <h4 class="text-uppercase"><strong>AMONESTACIONES</strong></h4>

                                    <p class="mb-0">

                                    <table width="100%">

                                        <tr>

                                            <th>NÚMERO</th>

                                            <th>FOTO</th>

                                            <th>NOMBRE</th>

                                            <th>APELLIDO</th>

                                            <th>CLUB</th>

                                            <th>AMARILLAS</th>

                                            <th>ROJAS</th>

                                        </tr>



                                        <?php

                                        foreach ($cautionedList as $player)

                                        { ?>

                                            <tr>

                                                <td><?php echo $player['number_player'] ?></td>
                                                <td>

                                                    <?php

                                                    $pathPhoto = 'admin/img/player/';

                                                    $photoPlayer = $pathPhoto . $player['id_player'] . '.jpg';

                                                    if (is_file($photoPlayer))

                                                    {

                                                        ?>

                                                        <img src="<?php echo $photoPlayer . '?' . time() ?>" style="max-height: 150px">

                                                    <?php } else

                                                    { ?>

                                                        <img src="img/no_photo.jpg" style="max-height: 150px">

                                                    <?php } ?>

                                                </td>

                                                <td><?php echo $player['name_player'] ?></td>
                                                <td><?php echo $player['lastname_player'] ?></td>
                                                <td><?php echo $player['lastname_player'] ?></td>


                                                <td><?php echo $player['yellow_cards'] ?></td>

                                                <td><?php echo $player['red_cards'] ?></td>

                                            </tr>

                                        <?php }

                                        ?>



                                    </table>

                                    </p>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>



                <hr>



                <div class="row">

                    <div class="col">

                        <section class="call-to-action featured featured-primary button-centered" style="">

                            <div class="col-12">

                                <div class="call-to-action-content">

                                    <h4 class="text-uppercase"><strong>RECEPCIONES</strong></h4>

                                    <p class="mb-0">

                                    <table width="100%">

                                        <tr>
                                            <th>NÚMERO</th>


                                            <th>FOTO</th>

                                            <th>NOMBRE</th>

                                            <th>APELLIDO</th>

                                            <th>CLUB</th>

                                            <th>RECEPCIONES</th>

                                        </tr>



                                        <?php

                                        $aux = 0;
                                        
                                        // var_dump($listIdGoalkeepers);

                                        foreach ($listIdGoalkeepers as $idGoalkeeper)

                                        {

                                            $player = $playerModel->getById($idGoalkeeper);
                                            // var_dump($player);

                                            if($player){
                                                foreach ($player AS $id => $info){
                                                    $player['id_player']=$info['id_player'];
                                                    $player['name_player']=$info['name_player'];
                                                    $player['name_team'] = $info['name_team'];       
                                                }
                                            }

                                            ?>

                                            <tr>
                                                <td></td>
                                                <td>

                                                    <?php

                                                    $pathPhoto = 'admin/img/player/';

                                                    $photoPlayer = $pathPhoto . $player['id_player'] . '.jpg';

                                                    if (is_file($photoPlayer))

                                                    {

                                                        ?>

                                                        <img src="<?php echo $photoPlayer . '?' . time() ?>" style="max-height: 150px">

                                                    <?php } else

                                                    { ?>

                                                        <img src="img/no_photo.jpg" style="max-height: 150px">

                                                    <?php } ?>

                                                </td>

                                                <td><?php echo $player['name_player'] ?></td>
                                                
                                                <td></td>
                                                
                                                <td><?php echo $player['name_team'] ?></td>
                                                

                                                <td><?php echo $listGoals[$aux] ?></td>

                                            </tr>

                                            <?php $aux++;

                                        }

                                        ?>



                                    </table>

                                    </p>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>



                <hr>



                <div class="row">

                    <div class="col">

                        <section class="call-to-action featured featured-primary button-centered" style="">

                            <div class="col-12">

                                <div class="call-to-action-content">

                                    <h4 class="text-uppercase"><strong>NOTICIAS</strong></h4>

                                    <p class="mb-0">

                                    <table width="100%">

                                        <tr>

                                            <th>IMAGEN</th>

                                            <th>CONTENIDO</th>

                                        </tr>



                                        <?php

                                        foreach ($newsList as $news)

                                        {

                                            ?>

                                            <tr>

                                                <td width="40%">

                                                    <?php

                                                    $imagesPath = "admin/img/news/" . $_GET['id'] . '/';

                                                    $file = $imagesPath . $news['id_news'] . '.jpg';

                                                    if (is_file($file))

                                                        echo "<img src='$file' style='max-height: 200px; max-width: 300px'>";

                                                    else

                                                        echo '-- SIN IMAGEN --';

                                                    ?>

                                                </td>

                                                <td><?php echo $news['text_news'] ?></td>

                                            </tr>

                                        <?php }

                                        ?>



                                    </table>

                                    </p>

                                </div>

                            </div>

                        </section>

                    </div>

                </div>



            </div>

        </section>





    </div>



    <?php include 'footer.php' ?>

</div>



<!-- Vendor -->

<script src="vendor/jquery/jquery.min.js"></script>

<script src="vendor/jquery.appear/jquery.appear.min.js"></script>

<script src="vendor/jquery.easing/jquery.easing.min.js"></script>

<script src="vendor/jquery-cookie/jquery-cookie.min.js"></script>

<script src="vendor/popper/umd/popper.min.js"></script>

<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<script src="vendor/common/common.min.js"></script>

<script src="vendor/jquery.validation/jquery.validation.min.js"></script>

<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<script src="vendor/jquery.gmap/jquery.gmap.min.js"></script>

<script src="vendor/jquery.lazyload/jquery.lazyload.min.js"></script>

<script src="vendor/isotope/jquery.isotope.min.js"></script>

<script src="vendor/owl.carousel/owl.carousel.min.js"></script>

<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<script src="vendor/vide/vide.min.js"></script>



<!-- Theme Base, Components and Settings -->

<script src="js/theme.js"></script>



<!-- Theme Custom -->

<script src="js/custom.js"></script>



<!-- Theme Initialization Files -->

<script src="js/theme.init.js"></script>



<script src="admin/plugins/lightslider/js/lightslider.js"></script>



<script>



    $(document).ready(function () {

        $(".lightSlider").lightSlider({

            gallery: true,

            item: 1,

            loop: true,

            slideMargin: 0,

            adaptiveHeight: true,

            responsive: [

                {

                    breakpoint: 800,

                    settings: {

                        thumbItem: 3

                    }

                },

                {

                    breakpoint: 480,

                    settings: {

                        thumbItem: 2

                    }

                }

            ]

        });

    });



</script>



</body>

</html>

