<?php
require "admin/views/phpqrcode/qrlib.php";    
include_once('admin/configs.php');
// include_once('../session_manager.php');
include('admin/connection.php');
include('admin/models/model_credential.php');
include('admin/models/model_player.php');
include('admin/models/model_team.php');

$teamModel = new Team_Model();
$playerModel = new Player_Model();
$credentialModel = new Credential_Model();

if (isset($_GET['id'])){
    $player = $playerModel->getById($_GET['id']);

    $credential = $credentialModel->getByPlayerId($_GET['id']);
    if($credential){
        foreach ($credential AS $id => $info){           
           $credential['id_credential']= $info['id_credential'];
           $credential['id_player']= $info['id_player'];
           $credential['date_begin_credential']=$info['date_begin_credential'];
           $credential['date_end_credential']=$info['date_end_credential']; 
           $credential['field_1_credential']=$info['field_1_credential'];
           $credential['field_2_credential']=$info['field_2_credential'];
           $credential['field_3_credential']=$info['field_3_credential'];
      
           $dd1=$info['field_1_credential'];
           $dd2=$info['field_2_credential'];
           $dd3=$info['field_3_credential'];
          }


          if($player){
            foreach ($player AS $id => $info){ 
                $player['id_player']=$info['id_player'];
                $player['name_player'] = $info['name_player'];
                $player['lastname_player'] = $info['lastname_player'];
                $player['lastname2_player'] = $info['lastname2_player'];
                $dd1=$info[$dd1];
                $dd2=$info[$dd2];
                $dd3=$info[$dd3];
              }
         }
    
     }

    
       if($credential==0){
         $coco=0;
       }else{
        $coco=1;
       }
    
$dir = 'temp/';
	
if (!file_exists($dir))
    mkdir($dir);

$filename = $dir.'test.png';

$tamaño = 4; //Tamaño de Pixel
$level = 'L'; //Precisión Baja
$framSize = 1; //Tamaño en blanco

}else{
    header('Location: list_player.php');
}



if (!$credential)
    $credential = $credentialModel->getEmpty();

$message = '';
if (count($_POST) > 0)
{
    include('../util/validators.php');
    $validator = new Validators();
    $message .= $validator->isValidDate($_POST['date_begin_player_credential'], 'Fecha de habilitación');
    $message .= $validator->isValidDate($_POST['date_end_player_credential'], 'Fecha de vencimiento');
    $message .= $validator->isNotEmpty($_POST['field_1_credential'], 'Dato 1');
    $message .= $validator->isNotEmpty($_POST['field_2_credential'], 'Dato 2');
    $message .= $validator->isNotEmpty($_POST['field_3_credential'], 'Dato 3');

    if (empty($message))
    {
        if ( empty($credential['id_credential']) )
        {
            $idCredential = $credentialModel->save(
                $_GET['id'],
                $_POST['field_1_credential'],
                $_POST['field_2_credential'],
                $_POST['field_3_credential'],
                $_POST['date_begin_player_credential'],
                $_POST['date_end_player_credential']
            );
        } else
        {
            if ($credentialModel->update(
                $credential['id_credential'],
                $_POST['field_1_credential'],
                $_POST['field_2_credential'],
                $_POST['field_3_credential'],
                $_POST['date_begin_player_credential'],
                $_POST['date_end_player_credential']

            ))
                $idCredential = $credential['id_credential'];
        }

        if ($idCredential)
            header('Location: list_player.php');
        else
            $message = 'Ocurrio un error al registrar el credencial del atleta.';
    } else
    {
        $credential['date_begin_player_credential'] = $_POST['date_begin_player_credential'];
        $credential['date_end_player_credential'] = $_POST['date_end_player_credential'];
        $credential['field_1_credential'] = $_POST['field_1_credential'];
        $credential['field_2_credential'] = $_POST['field_2_credential'];
        $credential['field_3_credential'] = $_POST['field_3_credential'];
    }
}

$options = array();
$options['Fecha de Nacimiento'] = 'born_date_player';
$options['Edad'] = 'code_age';
$options['Función'] = 'function_player';
$options['Carnet de Identidad'] = 'ci_player';
$options['País'] = 'country_player';
$options['Ciudad'] = 'city_player';
$options['Club'] = 'club_player';
$options['Deporte'] = 'sport_player';
$options['Categoría'] = 'category_player';
$options['Género'] = 'gender_player';
$options['División'] = 'division_player';
$options['Torneo'] = 'tournament_player';
$options['Nivel'] = 'level_player';
$options['Tipo de sangre'] = 'blood_type_player';
$options['Altura'] = 'height_player';
$options['Peso'] = 'weight_player';
$options['IMC'] = 'code_imc';
$options['Email'] = 'email_player';
$options['Teléfono'] = 'cellphone_player';
$options['Nombre de referencia'] = 'name_reference_player';
$options['Teléfono de referencia'] = 'phone_reference_player';
$options['Curriculum Deportivo'] = 'curriculum_vitae_player';



?>





<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo WEB_TITLE; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
   
   <link rel="STYLESHEET" type="text/css" href="micss1.css" media="print">
   <link rel="STYLESHEET" type="text/css" href="micss.css" >
</head>

<body >

            <div class="row">
                  <div class="col-md-4">
                 
                 </div>
                 
                 <div class="col-md-4">
                 
                 
            <div id="frente3">
                     <div class="row">
                     <div class="col-md-12">

                        <?php
                                    $photoPlayer = 'admin/img/player/1407.jpg';
                                    if (is_file($photoPlayer)){ 
                                        ?>
                                        <img src="<?php echo $photoPlayer?>" style="height: 130px;
    width: 120px;
    margin-top: 5px;
    border: 2px solid;
    border-color: black;
    border-radius: 10%;" alt="">
                                    <?php }else{ ?>

                                        <img src="<?php echo LOCALHOST;?>/img/icons/user-icon.png" style="height: 130px;
    width: 120px;
    margin-top: 5px;
    border: 2px solid;
    border-color: black;
    border-radius: 10%;" alt="">
                                        <?php } ?>

                   
                     </div>
                     </div>
               

                    <div class="row" style="margin-block-end: -5px;">
                      
                    </div>
    
              <footer class="row">
                <li  style=" list-style: none; text-align: center;font-weight: 600;padding-top: 5px; ">Santa Cruz - Bolivia</li>        
              </footer>
                     </div>
                 </div>

                 <div class="col-md-4">
                 
                 </div>



</div>


</body>
</html>