<footer id="footer">
    <?php include 'advertising.php'?>

    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-1">
                    <a href="index.php" class="logo">
                        <img alt="Logo Forward" class="img-fluid" src="admin/img/logos/logo.jpg?<?php echo time()?>" style="max-height: 100px">
                    </a>
                </div>
                <div class="col-lg-7">
                    <p>© Derechos reservados 2019.</p>
                </div>
                <div class="col-lg-4">
                </div>
            </div>
        </div>
    </div>
</footer>